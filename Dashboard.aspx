﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Dashboard.aspx.vb" Inherits="Dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <%--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous" />--%>
    <link href="fontawesome/css/all.css" rel="stylesheet" />
    <%--<link href="https://pro.fontawesome.com/releases/v5.1.0/css/all.css" rel="stylesheet" />--%>
    <style>
        .table thead {
            text-align: center;
            vertical-align: middle;
        }

            .table thead tr th {
                text-align: center;
                vertical-align: middle;
            }

        .table tr th {
            text-align: center;
        }

        .loader {
            border: 5px solid #f3f3f3;
            border-radius: 50%;
            border-top: 5px solid #073763;
            width: 25px;
            height: 25px;
            -webkit-animation: spin 1s linear infinite; /* Safari */
            animation: spin 1s linear infinite;
        }
        .table td div {
            margin:auto
        }
        .table th div {
            margin:auto
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
    <script>
        function GetCSV(tipo) {
            $.ajax({
                type: "POST", // Tipo de llamada
                beforeSend: function () {
                    $("#Down"+ tipo).html("<div id='download" + tipo + "'></div>");
                    $("#download" + tipo).addClass("loader");
                    console.log("Enviando petición de descarga "+ tipo);
                },
                async: true,
                url: "Dashboard.aspx/CreateCSVfile",
                data: '{Tipo: "' + tipo + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var datos = '<i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('+ "'" + tipo + "'" +');">';
                    console.log(datos);
                    $("#Down" + tipo).html(datos);
                    window.open(result.d, "_blank");
                    //alert("Prueba exitosa");

                },
                
            });
        }
    </script>
</head>
<body>
    <div class="container">
        <form id="form1" runat="server">

            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    <div class="text-center">
                        <h1>Analytics Strategy</h1>
                    </div>
                </div>
                <div class="col-4"></div>
            </div>
            <div class="row">
                <div class="col-9"></div>
                <div class="col-3">
                    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                        <i class="fa fa-calendar"></i>
                        &nbsp;
                <span></span>
                        <i class="fa fa-caret-down"></i>
                    </div>
                </div>
            </div>
            <br />
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead style="background-color: #073763; color: #FFF;">
                        <tr>
                            <th scope="col">Dato a Mostrar</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Filtros</th>
                            <th scope="col">Source</th>
                            <th scope="col">Fecha a tomar</th>
                            <th scope="col">Descarga</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row" id="totalleads"><%=TotalLeads %></th>
                            <td>Total Leads</td>
                            <td>Count de todos los idTickets</td>
                            <td>CuboBDLeads</td>
                            <td>Createdat_fix</td>
                            <td style="text-align:center; vertical-align: middle;"><a href="#" id="DownTotalLeads"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('TotalLeads'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="realleads"><%=RealLeads %></th>
                            <td>Real Leads</td>
                            <td>Count de todos los idtickets - duplicados - test</td>
                            <td>CuboBDLeads</td>
                            <td>Createdat_fix</td>
                            <td style="text-align:center"><a href="#" id="DownRealLeads"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('RealLeads'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="onlineapplyusers"><%=OnlineApplyUsers %></th>
                            <td>Online apply users</td>
                            <td>Real Leads + Groups = "AUTENTICACION" or "DATA INPUT"</td>
                            <td>CuboBDLeads</td>
                            <td>Createdat_fix</td>
                            <td style="text-align:center"><a href="#" id="DownOnlineApplyUsers"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OnlineApplyUsers'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="onlineauthenticatedusers"><%=OnlineAuthenticatedUsers %></th>
                            <td>Online authenticated users</td>
                            <td>Online apply users + AuthenticationStatus = "EXCLUSIÓN" OR "AUTENTICADO"  + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads</td>
                            <td>AuthenticatedDate</td>
                            <td style="text-align:center"><a href="#" id="DownOnlineAuthenticatedUsers"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OnlineAuthenticatedUsers'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="onlineauthenticated600"><%=OnlineAutenticated600 %></th>
                            <td>Online authenticated users >600</td>
                            <td>Online apply users + result = "VENTA" + AuthenticationStatus = "AUTENTICADO"  + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads</td>
                            <td>AuthenticatedDate</td>
                            <td style="text-align:center"><a href="#" id="DownOnlineAutenticated600"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OnlineAutenticated600'); return false"></i></a></td>
                        </tr>
                         <tr>
                            <th scope="row" id="onlineappscreated"><%=OnlineAppsCreated %></th>
                            <td>Online apps created</td>
                            <td>Count de BDApps de esos Online authenticated users >600 + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>AuthenticatedDate</td>
                            <td style="text-align:center"><a href="#" id="DownOnlineAppsCreated"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OnlineAppsCreated'); return false"></i></a></td>
                        </tr>                        
                        <tr>
                            <th scope="row" id="onlineappssubmitted"><%=OnlineAppsSubmitted %></th>
                            <td>Online apps submitted</td>
                            <td>Count de BDApps con ApplyResult NO VACÍO + Real Leads + Groups = "AUTENTICACION" or "DATA INPUT"  + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>ApplyDate</td>
                            <td style="text-align:center"><a href="#" id="DownOnlineAppsSubmitted"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OnlineAppsSubmitted'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="onlineappssubmitedamex"><%=OnlineAppsSubmittedAmex %></th>
                            <td>Online apps submitted AMEX</td>
                            <td>Count de BDApps de AMEX con ApplyResult = "SOLICITADA" + Real Leads + Groups = "AUTENTICACION" or "DATA INPUT"  + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>ApplyDate</td>
                            <td style="text-align:center"><a href="#" id="DownOnlineAppsSubmittedAmex"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OnlineAppsSubmittedAmex'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="onlineappssubmittedscotia"><%=OnlineAppsSubmittedScotia %></th>
                            <td>Online apps submitted Scotia</td>
                            <td>Count de BDApps de Scotia con ApplyResult NO VACÍO + Real Leads + Groups = "AUTENTICACION" or "DATA INPUT"  + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>ApplyDate</td>
                            <td style="text-align:center"><a href="#" id="DownOnlineAppsSubmittedScotia"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OnlineAppsSubmittedScotia'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="onlineappssubmittedciti"><%=OnlineAppsSubmittedCiti %></th>
                            <td>Online apps submitted Citi</td>
                            <td>Count de BDApps de Citi con ApplyResult NO VACÍO + Real Leads + Groups = "AUTENTICACION" or "DATA INPUT"  + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>ApplyDate</td>
                            <td style="text-align:center"><a href="#" id="DownOnlineAppsSubmittedCiti"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OnlineAppsSubmittedCiti'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="onlineappssubmittedHSBC"><%=OnlineAppsSubmittedHSBC %></th>
                            <td>Online apps submitted HSBC</td>
                            <td>Count de BDApps de HSBC con ApplyResult NO VACÍO + Real Leads + Groups = "AUTENTICACION" or "DATA INPUT"  + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>ApplyDate</td>
                            <td style="text-align:center"><a href="#" id="DownOnlineAppsSubmittedHSBC"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OnlineAppsSubmittedHSBC'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="onlineappsapprovedScotia"><%=OnlineAppsApprovedScotia %></th>
                            <td>Online apps approved Scotia</td>
                            <td>Count de BDApps de Scotia con ApplyResult = "APROBADA" + Real Leads + Groups = "AUTENTICACION" or "DATA INPUT"  + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>ApplyDate</td>
                            <td style="text-align:center"><a href="#" id="DownOnlineAppsApprovedScotia"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OnlineAppsApprovedScotia'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="onlineappsapprovedCiti"><%=OnlineAppsApprovedCiti %></th>
                            <td>Online apps approved Citi</td>
                            <td>Count de BDApps de Citi con ApplyResult = "APROBADA" + Real Leads + Groups = "AUTENTICACION" or "DATA INPUT"  + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>ApplyDate</td>
                            <td style="text-align:center"><a href="#" id="DownOnlineAppsApprovedCiti"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OnlineAppsApprovedCiti'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="onlineappsapprovedHSBC"><%=OnlineAppsApprovedHSBC %></th>
                            <td>Online apps approved HSBC</td>
                            <td>Count de BDApps de HSBC con ApplyResult = "APROBADA" + Real Leads + Groups = "AUTENTICACION" or "DATA INPUT"  + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>ApplyDate</td>
                            <td style="text-align:center"><a href="#" id="DownOnlineAppsApprovedHSBC"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OnlineAppsApprovedHSBC'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="amexshortleads"><%=AmexShortLeads %></th>
                            <td>Amex Short Leads</td>
                            <td>Real Leads + AmexShortLeads = "True"</td>
                            <td>CuboBDLeads</td>
                            <td>ApplyDate Short Lead</td>
                            <td style="text-align:center"><a href="#" id="DownAmexShortLeads"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('AmexShortLeads'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="leadsforoperations"><%=LeadsForOperations %></th>
                            <td>Leads for operations</td>
                            <td>Real Leads + Groups = "VENTAS CREDIT CARDS" + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads</td>
                            <td>Createdat_fix</td>
                            <td style="text-align:center"><a href="#" id="DownLeadsForOperations"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('LeadsForOperations'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="campaings"><%=Campaings %></th>
                            <td>15H, 15SH, 7-15H</td>
                            <td>Real Leads + Groups = "VENTAS CREDIT CARDS" + Campaign = "15-H" or "15-SH" or "715-H" + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads</td>
                            <td>Createdat_fix</td>
                            <td style="text-align:center"><a href="#" id="DownCampaings"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('Campaings'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="seguimientonocontactado"><%=SeguimientoNoContactado %></th>
                            <td>En Seguimiento / No Contactado</td>
                            <td>Real Leads + Groups = "VENTAS CREDIT CARDS" + Campaign = "15-H" or "15-SH" or "715-H" + Result = "SEGUIMIENTO" or "SIN ATENDER"  + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads</td>
                            <td>Createdat_fix</td>
                            <td style="text-align:center"><a href="#" id="DownSeguimientoNoContactado"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('SeguimientoNoContactado'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="rechazos"><%=Rechazos %></th>
                            <td>Rechazos</td>
                            <td>Real Leads + Groups = "VENTAS CREDIT CARDS" + Campaign = "15-H" or "15-SH" or "715-H" + Result = "RECHAZO CC"  + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads</td>
                            <td>Createdat_fix</td>
                            <td style="text-align:center"><a href="#" id="DownRechazos"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('Rechazos'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="ventausuario"><%=VentaUsuario %></th>
                            <td>Venta (Usuarios)</td>
                            <td>Real Leads + Groups = "VENTAS CREDIT CARDS" + Campaign = "15-H" or "15-SH" or "715-H" + Result = "VENTA"  + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads</td>
                            <td>Createdat_fix</td>
                            <td style="text-align:center"><a href="#" id="DownVentaUsuario"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('VentaUsuario'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="offlineappssubmitted"><%=OfflineAppsSubmitted %></th>
                            <td>Offline apps submited</td>
                            <td>Count de BDApps con ApplyResult NO VACÍO + Real Leads + Groups = "VENTAS CREDIT CARDS"  + AmexShortLeads = "False"</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>ApplyDate</td>
                            <td style="text-align:center"><a href="#" id="DownOfflineAppsSubmitted"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OfflineAppsSubmitted'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="offlineappssubmittedamex"><%=OfflineAppsSubmittedAmex %></th>
                            <td>Offline apps submited Amex</td>
                            <td>Count de BDApps de AMEX con ApplyResult = "SOLICITADA" + Leads for operations</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>ApplyDate</td>
                            <td style="text-align:center"><a href="#" id="DownOfflineAppsSubmittedAmex"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OfflineAppsSubmittedAmex'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="offlineappssubmittedscotia"><%=OfflineAppsSubmittedScotia %></th>
                            <td>Offline apps submited Scotia</td>
                            <td>Count de BDApps de Scotia con ApplyResult NO VACÍO + Leads for operations</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>ApplyDate</td>
                            <td style="text-align:center"><a href="#" id="DownOfflineAppsSubmittedScotia"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OfflineAppsSubmittedScotia'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="offlineappssubmittedhsbc"><%=OfflineAppsSubmittedHSBC %></th>
                            <td>Offline apps submited HSBC</td>
                            <td>Count de BDApps de HSBC con ApplyResult NO VACÍO + Leads for operations</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>ApplyDate</td>
                            <td style="text-align:center"><a href="#" id="DownOfflineAppsSubmittedHSBC"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OfflineAppsSubmittedHSBC'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="offlineappsapprovedscotia"><%=OfflineAppsApprovedScotia %></th>
                            <td>Offline apps approved Scotia</td>
                            <td>Count de BDApps de Scotia con ApplyResult = "APROBADA" + Leads for operations</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>ApplyDate</td>
                            <td style="text-align:center"><a href="#" id="DownOfflineAppsApprovedScotia"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OfflineAppsApprovedScotia'); return false"></i></a></td>
                        </tr>
                        <tr>
                            <th scope="row" id="offlineappsapprovedhsbc"><%=OfflineAppsApprovedHSBC %></th>
                            <td>Offline apps approved HSBC</td>
                            <td>Count de BDApps de HSBC con ApplyResult = "APROBADA" + Leads for operations</td>
                            <td>CuboBDLeads + CuboBDApps</td>
                            <td>ApplyDate</td>
                            <td style="text-align:center"><a href="#" id="DownOfflineAppsApprovedHSBC"><i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('OfflineAppsApprovedHSBC'); return false"></i></a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</body>
</html>

<script type="text/javascript">
    $(function () {

        var start = moment().startOf('month');
        var end = moment().endOf('month');

        function cb(start, end) {
            $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        }

        $(function () {
            $('#reportrange').daterangepicker({
                "locale": {
                    "format": "DD-MM-YYYY",
                    "separator": " - ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },
                "startDate": start,
                "endDate": end,
                "opens": "left"
            }, cb);
        });

        cb(start, end);

        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalleads').html('<div id="loader1"></div>');
                $('#loader1').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "Dashboard.aspx/TotalLeads_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalleads').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#realleads').html('<div id="loader2"></div>');
                $('#loader2').addClass('loader');
                console.log("Enviando petición de Real Leads");
            },
            async: true,
            url: "Dashboard.aspx/RealLeads_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                //console.log(result.d);
                $('#realleads').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineapplyusers').html('<div id="loader3"></div>');
                $('#loader3').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineApplyUsers_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineapplyusers').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineauthenticatedusers').html('<div id="loader4"></div>');
                $('#loader4').addClass('loader');
                console.log("Enviando petición de Online Authenticated Users")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAuthenticatedUsers_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineauthenticatedusers').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineauthenticated600').html('<div id="loader5"></div>');
                $('#loader5').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAutenticated600_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineauthenticated600').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappscreated').html('<div id="loader6"></div>');
                $('#loader6').addClass('loader');
                console.log("Enviando petición de Online Apps Created")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsCreated_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappscreated').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappssubmitted').html('<div id="loader7"></div>');
                $('#loader7').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsSubmitted_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappssubmitted').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappssubmitedamex').html('<div id="loader8"></div>');
                $('#loader8').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsSubmittedAmex_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappssubmitedamex').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappssubmittedscotia').html('<div id="loader9"></div>');
                $('#loader9').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsSubmittedScotia_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappssubmittedscotia').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappssubmittedciti').html('<div id="loader10"></div>');
                $('#loader10').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsSubmittedCiti_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappssubmittedciti').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappssubmittedHSBC').html('<div id="loader11"></div>');
                $('#loader11').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsSubmittedHSBC_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappssubmittedHSBC').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappsapprovedScotia').html('<div id="loader12"></div>');
                $('#loader12').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsApprovedScotia_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappsapprovedScotia').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappsapprovedCiti').html('<div id="loader13"></div>');
                $('#loader13').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsApprovedCiti_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappsapprovedCiti').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappsapprovedHSBC').html('<div id="loader14"></div>');
                $('#loader14').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsApprovedHSBC_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappsapprovedHSBC').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#amexshortleads').html('<div id="loader15"></div>');
                $('#loader15').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/AmexShortLeads_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#amexshortleads').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#leadsforoperations').html('<div id="loader16"></div>');
                $('#loader16').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/LeadsForOperations_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#leadsforoperations').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#campaings').html('<div id="loader17"></div>');
                $('#loader17').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/Campaings_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#campaings').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#seguimientonocontactado').html('<div id="loader18"></div>');
                $('#loader18').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/SeguimientoNoContactado_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#seguimientonocontactado').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#rechazos').html('<div id="loader19"></div>');
                $('#loader19').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/Rechazos_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#rechazos').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#ventausuario').html('<div id="loader20"></div>');
                $('#loader20').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/VentaUsuario_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#ventausuario').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#offlineappssubmitted').html('<div id="loader21"></div>');
                $('#loader21').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OfflineAppsSubmitted_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#offlineappssubmitted').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#offlineappssubmittedamex').html('<div id="loader22"></div>');
                $('#loader22').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OfflineAppsSubmittedAmex_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#offlineappssubmittedamex').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#offlineappssubmittedscotia').html('<div id="loader23"></div>');
                $('#loader23').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OfflineAppsSubmittedScotia_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#offlineappssubmittedscotia').html(result.d);
            }
        });
        
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#offlineappssubmittedhsbc').html('<div id="loader24"></div>');
                $('#loader24').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OfflineAppsSubmittedHSBC_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#offlineappssubmittedhsbc').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#offlineappsapprovedscotia').html('<div id="loader25"></div>');
                $('#loader25').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OfflineAppsApprovedScotia_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#offlineappsapprovedscotia').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#offlineappsapprovedhsbc').html('<div id="loader26"></div>');
                $('#loader26').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OfflineAppsApprovedHSBC_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#offlineappsapprovedhsbc').html(result.d);
            }
        });
    });

    $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalleads').html('<div id="loader1"></div>');
                $('#loader1').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "Dashboard.aspx/TotalLeads_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalleads').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#realleads').html('<div id="loader2"></div>');
                $('#loader2').addClass('loader');
                console.log("Enviando petición de Real Leads");
            },
            async: true,
            url: "Dashboard.aspx/RealLeads_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                //console.log(result.d);
                $('#realleads').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineapplyusers').html('<div id="loader3"></div>');
                $('#loader3').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineApplyUsers_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineapplyusers').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineauthenticatedusers').html('<div id="loader4"></div>');
                $('#loader4').addClass('loader');
                console.log("Enviando petición de Online Authenticated Users")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAuthenticatedUsers_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineauthenticatedusers').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineauthenticated600').html('<div id="loader5"></div>');
                $('#loader5').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAutenticated600_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineauthenticated600').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappscreated').html('<div id="loader6"></div>');
                $('#loader6').addClass('loader');
                console.log("Enviando petición de Online Apps Created")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsCreated_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappscreated').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappssubmitted').html('<div id="loader7"></div>');
                $('#loader7').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsSubmitted_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappssubmitted').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappssubmitedamex').html('<div id="loader8"></div>');
                $('#loader8').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsSubmittedAmex_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappssubmitedamex').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappssubmittedscotia').html('<div id="loader9"></div>');
                $('#loader9').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsSubmittedScotia_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappssubmittedscotia').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappssubmittedciti').html('<div id="loader10"></div>');
                $('#loader10').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsSubmittedCiti_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappssubmittedciti').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappssubmittedHSBC').html('<div id="loader11"></div>');
                $('#loader11').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsSubmittedHSBC_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappssubmittedHSBC').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappsapprovedScotia').html('<div id="loader12"></div>');
                $('#loader12').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsApprovedScotia_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappsapprovedScotia').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappsapprovedCiti').html('<div id="loader13"></div>');
                $('#loader13').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsApprovedCiti_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappsapprovedCiti').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#onlineappsapprovedHSBC').html('<div id="loader14"></div>');
                $('#loader14').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OnlineAppsApprovedHSBC_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#onlineappsapprovedHSBC').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#amexshortleads').html('<div id="loader15"></div>');
                $('#loader15').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/AmexShortLeads_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#amexshortleads').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#leadsforoperations').html('<div id="loader16"></div>');
                $('#loader16').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/LeadsForOperations_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#leadsforoperations').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#campaings').html('<div id="loader17"></div>');
                $('#loader17').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/Campaings_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#campaings').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#seguimientonocontactado').html('<div id="loader18"></div>');
                $('#loader18').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/SeguimientoNoContactado_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#seguimientonocontactado').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#rechazos').html('<div id="loader19"></div>');
                $('#loader19').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/Rechazos_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#rechazos').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#ventausuario').html('<div id="loader20"></div>');
                $('#loader20').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/VentaUsuario_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#ventausuario').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#offlineappssubmitted').html('<div id="loader21"></div>');
                $('#loader21').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OfflineAppsSubmitted_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#offlineappssubmitted').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#offlineappssubmittedamex').html('<div id="loader22"></div>');
                $('#loader22').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OfflineAppsSubmittedAmex_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#offlineappssubmittedamex').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#offlineappssubmittedscotia').html('<div id="loader23"></div>');
                $('#loader23').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OfflineAppsSubmittedScotia_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#offlineappssubmittedscotia').html(result.d);
            }
        });

        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#offlineappssubmittedhsbc').html('<div id="loader24"></div>');
                $('#loader24').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OfflineAppsSubmittedHSBC_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#offlineappssubmittedhsbc').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#offlineappsapprovedscotia').html('<div id="loader25"></div>');
                $('#loader25').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OfflineAppsApprovedScotia_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#offlineappsapprovedscotia').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#offlineappsapprovedhsbc').html('<div id="loader26"></div>');
                $('#loader26').addClass('loader');
                console.log("Enviando petición de Conversion Rate")
            },
            async: true,
            url: "Dashboard.aspx/OfflineAppsApprovedHSBC_",
            data: '{Desde: "' + picker.startDate.format('YYYY-MM-DD') + '", Hasta: "' + picker.endDate.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#offlineappsapprovedhsbc').html(result.d);
            }
        });
    });
</script>
