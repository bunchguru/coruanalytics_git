﻿Imports System.Data
Imports System.IO
Imports System.Threading
Imports System.Web.Services
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports Newtonsoft.Json
Imports Excel = Microsoft.Office.Interop.Excel
Imports ClosedXML
Partial Class DashboardScrapper
    Inherits System.Web.UI.Page

    Public Shared TotalLeads, TotalAmex, TotalBancomer, TotalBanamex, TotalScotia, TotalHsbc, TotalAmexAprobadas, TotalBancomerAprobadas, TotalBanamexAprobadas, TotalScotiaAprobadas, TotalHsbcAprobadas,
        TotalAmexRechazadas, TotalBancomerRechazadas, TotalBanamexRechazadas, TotalScotiaRechazadas, TotalHsbcRechazadas,
        TotalAmexError, TotalBancomerError, TotalBanamexError, TotalScotiaError, TotalHsbcError,
        TotalAmexNoProcesadas, TotalBancomerNoProcesadas, TotalBanamexNoProcesadas, TotalScotiaNoProcesadas, TotalHsbcNoProcesadas,
        RealLeads, ConversionRate, OnlineApplyUsers, OnlineAuthenticatedUsers, OnlineAutenticated, OnlineAutenticated600,
        SellabeLeadsConversionRate, OnlineAppsCreated, OnlineAppsSubmitted, OnlineAppsSubmittedAmex, OnlineAppsSubmittedScotia, OnlineAppsSubmittedCiti,
        OnlineAppsSubmittedHSBC, OnlineAppsApproved, OfflineAppsApprovedAmex, OfflineAppsApprovedScotia, OfflineAppsApprovedCiti,
        OfflineAppsApprovedHSBC, AmexShortLeads, LeadsForOperations, Campaings, SeguimientoNoContactado, Rechazos, VentaUsuario,
        VentaApps, VentaAppsAmex, VentaAppsScotia, OfflineAppsApproved, Amex, Scotia, ResuelveTuDeuda, FDesde, FHasta, OnlineAppsApprovedScotia,
        OnlineAppsApprovedCiti, OnlineAppsApprovedHSBC, OfflineAppsSubmitted, OfflineAppsSubmittedAmex, OfflineAppsSubmittedScotia,
        OfflineAppsSubmittedHSBC, Desde, Hasta,
        pamexaprobada, pbacnomeraprobada, pbanamexaprobada, pscotiaaprobada, phsbcaprobada,
        pamexrechazada, pbacnomerrechazada, pbanamexrechazada, pscotiarechazada, phsbcrechazada,
        pamexerror, pbacnomererror, pbanamexerror, pscotiaerror, phsbcerror,
        pamexnp, pbacnomernp, pbanamexnp, pscotianp, phsbcnp,
        pamexrep, pbacnomerrep, pbanamexrep, pscotiarep, phsbcrep,
        amexrep, bacnomerrep, banamexrep, scotiarep, hsbcrep As String

    Public Shared TotalLeadsQry, RealLeadsQey, strCommonColumnName As String
    Public Shared dTable As DataTable
    Public Function FirstDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Return New DateTime(sourceDate.Year, sourceDate.Month, 1)
    End Function

    'Get the last day of the month
    Public Function LastDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Dim lastDay As DateTime = New DateTime(sourceDate.Year, sourceDate.Month, 1)
        Return lastDay.AddMonths(1).AddDays(-1)
    End Function
    Private Sub Dashboard_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim Start As String = FirstDayOfMonth(Date.Now).ToString("yyyy-MM-dd")
        Dim Ends As String = LastDayOfMonth(Date.Now).ToString("yyyy-MM-dd")
    End Sub
    <WebMethod>
    Public Shared Function TotalAmex_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'AMEX' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'AMEX' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalAmex = dr(0)
                TotalAmex = FormatNumber(TotalAmex, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(TotalAmex, 0)
    End Function
    <WebMethod>
    Public Shared Function TotalBancomer_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'BANCOMER' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'BANCOMER' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalBancomer = dr(0)
                TotalBancomer = FormatNumber(TotalBancomer, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(TotalBancomer, 0)
    End Function
    <WebMethod>
    Public Shared Function TotalBanamex_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'CITIBANAMEX' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'CITIBANAMEX' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalBanamex = dr(0)
                TotalBanamex = FormatNumber(TotalBanamex, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(TotalBanamex, 0)
    End Function
    <WebMethod>
    Public Shared Function TotalHsbc_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'HSBC' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'HSBC' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalHsbc = dr(0)
                TotalHsbc = FormatNumber(TotalHsbc, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(TotalHsbc, 0)
    End Function
    <WebMethod>
    Public Shared Function TotalScotia_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'SCOTIABANK' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'SCOTIABANK' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalScotia = dr(0)
                TotalScotia = FormatNumber(TotalScotia, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(TotalScotia, 0)
    End Function
    <WebMethod>
    Public Shared Function TotalAmexAprobadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'AMEX' AND LogScrapper.Folio LIKE '%MXN%'  AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'AMEX' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalAmexAprobadas = dr(0)
                TotalAmexAprobadas = FormatNumber(TotalAmexAprobadas, 0)

            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        pamexaprobada = Math.Round(Integer.Parse(TotalAmexAprobadas.Replace(",", "")) / Integer.Parse(TotalAmex.Replace(",", "")) * 100, 2).ToString + " %"
        If pamexaprobada.Contains("NaN") Then
            pamexaprobada = "0 %"
        End If
        Return TotalAmexAprobadas.ToString + "_" + pamexaprobada
    End Function
    <WebMethod>
    Public Shared Function TotalBancomerAprobadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'BANCOMER' AND LogScrapper.Folio LIKE '%No. de folio%' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'BANCOMER' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalBancomerAprobadas = dr(0)
                TotalBancomerAprobadas = FormatNumber(TotalBancomerAprobadas, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalBancomerAprobadas, 0)
        pbacnomeraprobada = Math.Round(Integer.Parse(TotalBancomerAprobadas.Replace(",", "")) / Integer.Parse(TotalBancomer.Replace(",", "")) * 100, 2).ToString + " %"
        If pbacnomeraprobada.Contains("NaN") Then
            pbacnomeraprobada = "0 %"
        End If
        Return TotalBancomerAprobadas.ToString + "_" + pbacnomeraprobada
    End Function



    <WebMethod>
    Public Shared Function TotalBanamexAprobadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'CITIBANAMEX' AND LogScrapper.respuesta LIKE '%¡Felicidades%' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'CITIBANAMEX' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalBanamexAprobadas = dr(0)
                TotalBanamexAprobadas = FormatNumber(TotalBanamexAprobadas, 0)

            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalBanamexAprobadas, 0)
        pbanamexaprobada = Math.Round(Integer.Parse(TotalBanamexAprobadas.Replace(",", "")) / Integer.Parse(TotalBanamex.Replace(",", "")) * 100, 2).ToString + " %"
        If pbanamexaprobada.Contains("NaN") Then
            pbanamexaprobada = "0 %"
        End If
        Return TotalBanamexAprobadas.ToString + "_" + pbanamexaprobada
    End Function
    <WebMethod>
    Public Shared Function TotalHsbcAprobadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'HSBC' AND LogScrapper.Folio LIKE '%LEAP%'  AND " &
            "LogScrapper.comentario!=  'No obtuvimos respuesta del scrapper'  AND " &
            "LogScrapper.respuesta != 'SinRespuesta' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'HSBC' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalHsbcAprobadas = dr(0)
                TotalHsbcAprobadas = FormatNumber(TotalHsbcAprobadas, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalHsbcAprobadas, 0)
        phsbcaprobada = Math.Round(Integer.Parse(TotalHsbcAprobadas.Replace(",", "")) / Integer.Parse(TotalHsbc.Replace(",", "")) * 100, 2).ToString + " %"
        If phsbcaprobada.Contains("NaN") Then
            phsbcaprobada = "0 %"
        End If
        Return TotalHsbcAprobadas.ToString + "_" + phsbcaprobada
    End Function
    <WebMethod>
    Public Shared Function TotalScotiaAprobadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'SCOTIABANK' AND LogScrapper.comentario LIKE '%SUCCESS%'  AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'SCOTIABANK' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalScotiaAprobadas = dr(0)
                TotalScotiaAprobadas = FormatNumber(TotalScotiaAprobadas, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalScotiaAprobadas, 0)
        pscotiaaprobada = Math.Round(Integer.Parse(TotalScotiaAprobadas.Replace(",", "")) / Integer.Parse(TotalScotia.Replace(",", "")) * 100, 2).ToString + " %"
        If pscotiaaprobada.Contains("NaN") Then
            pscotiaaprobada = "0 %"
        End If
        Return TotalScotiaAprobadas.ToString + "_" + pscotiaaprobada
    End Function
    <WebMethod>
    Public Shared Function TotalAmexRechazadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'AMEX' AND  LogScrapper.Folio like '%SinFolio%' and LogScrapper.respuesta != 'SinRespuesta' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'AMEX' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalAmexRechazadas = dr(0)
                TotalAmexRechazadas = FormatNumber(TotalAmexRechazadas, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        pamexrechazada = Math.Round(Integer.Parse(TotalAmexRechazadas.Replace(",", "")) / Integer.Parse(TotalAmex.Replace(",", "")) * 100, 2).ToString + " %"
        If pamexrechazada.Contains("NaN") Then
            pamexrechazada = "0 %"
        End If
        Return TotalAmexRechazadas.ToString + "_" + phsbcaprobada
        'Return FormatNumber(TotalAmexRechazadas, 0)
    End Function
    <WebMethod>
    Public Shared Function TotalBancomerRechazadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'BANCOMER' AND LogScrapper.Folio like '%SinFolio%' and respuesta != 'SinRespuesta' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'BANCOMER' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalBancomerRechazadas = dr(0)
                TotalBancomerRechazadas = FormatNumber(TotalBancomerRechazadas, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalBancomerRechazadas, 0)
        pbacnomerrechazada = Math.Round(Integer.Parse(TotalBancomerRechazadas.Replace(",", "")) / Integer.Parse(TotalBancomer.Replace(",", "")) * 100, 2).ToString + " %"
        If pbacnomerrechazada.Contains("NaN") Then
            pbacnomerrechazada = "0 %"
        End If
        Return TotalBancomerRechazadas.ToString + "_" + pbacnomerrechazada
    End Function
    <WebMethod>
    Public Shared Function TotalBanamexRechazadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'CITIBANAMEX' AND LogScrapper.comentario !=  'No obtuvimos respuesta del scrapper' AND LogScrapper.Folio. LIKE '%SinFolio%' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'CITIBANAMEX' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalBanamexAprobadas = dr(0)
                TotalBanamexAprobadas = FormatNumber(TotalBanamexAprobadas, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalBanamexAprobadas, 0)
        pbanamexrechazada = Math.Round(Integer.Parse(TotalBanamexAprobadas.Replace(",", "")) / Integer.Parse(TotalBanamex.Replace(",", "")) * 100, 2).ToString + " %"
        If pbanamexrechazada.Contains("NaN") Then
            pbanamexrechazada = "0 %"
        End If
        Return TotalBanamexAprobadas.ToString + "_" + pbanamexrechazada
    End Function
    <WebMethod>
    Public Shared Function TotalHsbcRechazadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'HSBC' AND LogScrapper.Folio LIKE '%SinFolio%'  AND LogScrapper.comentario !=  'No obtuvimos respuesta del scrapper'  AND " &
            "LogScrapper.respuesta != 'SinRespuesta'AND LogScrapper.estatus != 'error' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'HSBC' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalHsbcRechazadas = dr(0)
                TotalHsbcRechazadas = FormatNumber(TotalHsbcRechazadas, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalHsbcRechazadas, 0)
        phsbcrechazada = Math.Round(Integer.Parse(TotalHsbcRechazadas.Replace(",", "")) / Integer.Parse(TotalHsbc.Replace(",", "")) * 100, 2).ToString + " %"
        If phsbcrechazada.Contains("NaN") Then
            phsbcrechazada = "0 %"
        End If
        Return TotalHsbcRechazadas.ToString + "_" + phsbcrechazada
    End Function
    <WebMethod>
    Public Shared Function TotalScotiaRechazadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'SCOTIABANK' AND LogScrapper.Folio LIKE '%SinFolio%' AND LogScrapper.respuesta != 'SinRespuesta' AND " &
            "LogScrapper.comentario != 'BAD FIELD' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'SCOTIABANK' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalScotiaRechazadas = dr(0)
                TotalScotiaRechazadas = FormatNumber(TotalScotiaRechazadas, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalScotiaRechazadas, 0)
        pscotiarechazada = Math.Round(Integer.Parse(TotalScotiaRechazadas.Replace(",", "")) / Integer.Parse(TotalScotia.Replace(",", "")) * 100, 2).ToString + " %"
        If pscotiarechazada.Contains("NaN") Then
            pscotiarechazada = "0 %"
        End If
        Return TotalScotiaRechazadas.ToString + "_" + pscotiarechazada
    End Function
    <WebMethod>
    Public Shared Function TotalAmexError_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'AMEX' AND  LogScrapper.Folio like '%SinFolio%' AND LogScrapper.respuesta LIKE '%Favor de corregir los errores%' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'AMEX' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalAmexError = dr(0)
                TotalAmexError = FormatNumber(TotalAmexError, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalAmexRechazadas, 0)
        pamexerror = Math.Round(Integer.Parse(TotalAmexRechazadas.Replace(",", "")) / Integer.Parse(TotalAmex.Replace(",", "")) * 100, 2).ToString + " %"
        If pamexerror.Contains("NaN") Then
            pamexerror = "0 %"
        End If
        Return TotalAmexRechazadas.ToString + "_" + pamexerror
    End Function
    <WebMethod>
    Public Shared Function TotalBancomerError_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'BANCOMER' AND LogScrapper.Folio like '%SinFolio%' and LogScrapper.respuesta LIKE '%error%' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'BANCOMER' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalBancomerError = dr(0)
                TotalBancomerError = FormatNumber(TotalBancomerError, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalBancomerError, 0)
        pbacnomererror = Math.Round(Integer.Parse(TotalBancomerError.Replace(",", "")) / Integer.Parse(TotalBancomer.Replace(",", "")) * 100, 2).ToString + " %"
        If pbacnomererror.Contains("NaN") Then
            pbacnomererror = "0 %"
        End If
        Return TotalBancomerError.ToString + "_" + pbacnomererror
    End Function
    <WebMethod>
    Public Shared Function TotalBanamexError_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'CITIBANAMEX' AND LogScrapper.comentario LIKE 'error' AND LogScrapper.Folio LIKE '%SinFolio%' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'CITIBANAMEX' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalBanamexError = dr(0)
                TotalBanamexError = FormatNumber(TotalBanamexError, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalBanamexError, 0)
        pbanamexerror = Math.Round(Integer.Parse(TotalBanamexError.Replace(",", "")) / Integer.Parse(TotalBanamex.Replace(",", "")) * 100, 2).ToString + " %"
        If pbanamexerror.Contains("NaN") Then
            pbanamexerror = "0 %"
        End If
        Return TotalBanamexError.ToString + "_" + pbanamexerror
    End Function
    <WebMethod>
    Public Shared Function TotalHsbcError_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'HSBC' AND LogScrapper.Folio LIKE '%SinFolio%'  AND LogScrapper.comentario LIKE  '%error en los campos%'  AND " &
            "LogScrapper.respuesta != 'SinRespuesta' AND LogScrapper.estatus = 'error' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'HSBC' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalHsbcError = dr(0)
                TotalHsbcError = FormatNumber(TotalHsbcError, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalHsbcError, 0)
        phsbcerror = Math.Round(Integer.Parse(TotalHsbcError.Replace(",", "")) / Integer.Parse(TotalHsbc.Replace(",", "")) * 100, 2).ToString + " %"
        If phsbcerror.Contains("NaN") Then
            phsbcerror = "0 %"
        End If
        Return TotalHsbcError.ToString + "_" + phsbcerror
    End Function
    <WebMethod>
    Public Shared Function TotalScotiaError_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'SCOTIABANK' AND LogScrapper.Folio LIKE '%SinFolio%' AND LogScrapper.comentario LIKE '%BAD FIELD%' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'SCOTIABANK' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalScotiaError = dr(0)
                TotalScotiaError = FormatNumber(TotalScotiaError, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalScotiaError, 0)
        pscotiaerror = Math.Round(Integer.Parse(TotalScotiaError.Replace(",", "")) / Integer.Parse(TotalScotia.Replace(",", "")) * 100, 2).ToString + " %"
        If pscotiaerror.Contains("NaN") Then
            pscotiaerror = "0 %"
        End If
        Return TotalScotiaError.ToString + "_" + pscotiaerror
    End Function
    <WebMethod>
    Public Shared Function TotalAmexNoProcesadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'AMEX' AND  LogScrapper.Folio like '%SinFolio%' AND LogScrapper.respuesta LIKE '%SinRespuesta%' AND " &
            "LogScrapper.estatus LIKE '%SinProceso%'AND LogScrapper.comentario LIKE '%SinComentario%' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'AMEX' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalAmexNoProcesadas = dr(0)
                TotalAmexNoProcesadas = FormatNumber(TotalAmexNoProcesadas, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalAmexNoProcesadas, 0)
        pamexnp = Math.Round(Integer.Parse(TotalAmexNoProcesadas.Replace(",", "")) / Integer.Parse(TotalAmex.Replace(",", "")) * 100, 2).ToString + " %"
        If pamexnp.Contains("NaN") Then
            pamexnp = "0 %"
        End If
        Return TotalAmexNoProcesadas.ToString + "_" + pamexnp
    End Function
    <WebMethod>
    Public Shared Function TotalBancomerNoProcesadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'BANCOMER' AND  LogScrapper.Folio like '%SinFolio%' AND LogScrapper.respuesta LIKE '%SinRespuesta%' AND " &
            "LogScrapper.estatus LIKE '%SinProceso%'AND LogScrapper.comentario LIKE '%SinComentario%' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'BANCOMER' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalBancomerNoProcesadas = dr(0)
                TotalBancomerNoProcesadas = FormatNumber(TotalBancomerNoProcesadas, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalBancomerNoProcesadas, 0)
        pbacnomernp = Math.Round(Integer.Parse(TotalBancomerNoProcesadas.Replace(",", "")) / Integer.Parse(TotalBancomer.Replace(",", "")) * 100, 2).ToString + " %"
        If pbacnomernp.Contains("NaN") Then
            pbacnomernp = "0 %"
        End If
        Return TotalBancomerNoProcesadas.ToString + "_" + pbacnomernp
    End Function
    <WebMethod>
    Public Shared Function TotalBanamexNoProcesadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'CITIBANAMEX' AND  LogScrapper.Folio like '%SinFolio%' AND LogScrapper.respuesta LIKE '%SinRespuesta%' AND " &
            "LogScrapper.estatus LIKE '%SinProceso%'AND LogScrapper.comentario LIKE '%SinComentario%' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'CITIBANAMEX' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalBanamexNoProcesadas = dr(0)
                TotalBanamexNoProcesadas = FormatNumber(TotalBanamexNoProcesadas, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalBanamexNoProcesadas, 0)
        pbanamexnp = Math.Round(Integer.Parse(TotalBanamexNoProcesadas.Replace(",", "")) / Integer.Parse(TotalBanamex.Replace(",", "")) * 100, 2).ToString + " %"
        If pbanamexnp.Contains("NaN") Then
            pbanamexnp = "0 %"
        End If
        Return TotalBanamexNoProcesadas.ToString + "_" + pbanamexnp
    End Function
    <WebMethod>
    Public Shared Function TotalHsbcNoProcesadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'HSBC' AND  LogScrapper.Folio like '%SinFolio%' AND LogScrapper.respuesta LIKE '%SinRespuesta%' AND " &
            "LogScrapper.estatus LIKE '%SinProceso%' AND LogScrapper.comentario LIKE '%SinComentario%' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'HSBC' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalHsbcNoProcesadas = dr(0)
                TotalHsbcNoProcesadas = FormatNumber(TotalHsbcNoProcesadas, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalHsbcNoProcesadas, 0)
        phsbcnp = Math.Round(Integer.Parse(TotalHsbcNoProcesadas.Replace(",", "")) / Integer.Parse(TotalHsbc.Replace(",", "")) * 100, 2).ToString + " %"
        If phsbcnp.Contains("NaN") Then
            phsbcnp = "0 %"
        End If
        Return TotalHsbcNoProcesadas.ToString + "_" + phsbcnp
    End Function
    <WebMethod>
    Public Shared Function getduplicados(ByVal Desde As String, ByVal Hasta As String) As String
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim comando As New MySqlCommand
        FDesde = Desde
        FHasta = Hasta
        comando.CommandText = "SELECT Banco, sum(y) as total FROM (SELECT Email, banco, count(banco) as y FROM `GuruLake`.`log_scrapper_online` WHERE  corrioScraper = 'si' and STR_TO_DATE(date_format(log_scrapper_online.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= @inicio and STR_TO_DATE(date_format(log_scrapper_online.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= @final GROUP BY email, banco HAVING COUNT(*) > 1 ) as x group by banco"
        comando.Parameters.AddWithValue("@inicio", FDesde)
        comando.Parameters.AddWithValue("@final", FHasta)

        comando.Connection = myConnection
        Dim da As New MySqlDataAdapter
        Dim ds As New DataSet
        amexrep = "0"
        bacnomerrep = "0"
        banamexrep = "0"
        hsbcrep = "0"
        scotiarep = "0"

        pamexrep = "0 %"
        pbacnomerrep = "0 %"
        pbanamexrep = "0 %"
        phsbcrep = "0 %"
        pscotiarep = "0 %"

        Try
            comando.Connection.Open()

            da.SelectCommand = comando
            da.Fill(ds, "duplicadas")
            comando.Connection.Close()

            For Each fila As DataRow In ds.Tables("duplicadas").Rows
                'porcentaje rechazadas
                Select Case fila("banco")
                    Case "AMEX"
                        amexrep = fila("total").ToString
                        pamexrep = Math.Round(((Integer.Parse(fila("total")) / Integer.Parse(TotalAmex.Replace(",", ""))) * 100)).ToString + " %"
                        If pamexrep.Contains("NaN") Then
                            pamexrep = "0 %"
                        End If
                    Case "CITIBANAMEX"
                        banamexrep = fila("total").ToString
                        pbanamexrep = Math.Round(((Integer.Parse(fila("total")) / Integer.Parse(TotalBanamex.Replace(",", ""))) * 100)).ToString + " %"
                        If pbanamexrep.Contains("NaN") Then
                            pbanamexrep = "0 %"
                        End If
                    Case "SCOTIABANK"
                        scotiarep = fila("total").ToString
                        pscotiarep = Math.Round(((Integer.Parse(fila("total")) / Integer.Parse(TotalScotia.Replace(",", ""))) * 100)).ToString + " %"
                        If pscotiarep.Contains("NaN") Then
                            pscotiarep = "0 %"
                        End If
                    Case "HSBC"
                        hsbcrep = fila("total").ToString
                        phsbcrep = Math.Round(((Integer.Parse(fila("total")) / Integer.Parse(TotalHsbc.Replace(",", ""))) * 100)).ToString + " %"
                        If phsbcrep.Contains("NaN") Then
                            phsbcrep = "0 %"
                        End If
                    Case "BANCOMER"
                        bacnomerrep = fila("total").ToString
                        pbacnomerrep = Math.Round(((Integer.Parse(fila("total")) / Integer.Parse(TotalBancomer.Replace(",", ""))) * 100)).ToString + " %"
                        If pbacnomerrep.Contains("NaN") Then
                            pbacnomerrep = "0 %"
                        End If

                End Select

            Next
        Catch ex As Exception
            If comando.Connection.State = ConnectionState.Open Then
                comando.Connection.Close()
            End If
        End Try
        Dim cadena_devolver = amexrep.ToString + "_" + bacnomerrep.ToString + "_" + banamexrep.ToString + "_" + hsbcrep.ToString + "_" + scotiarep.ToString + "&" + pamexrep.ToString + "_" + pbacnomerrep.ToString + "_" + pbanamexrep.ToString + "_" + phsbcrep.ToString + "_" + pscotiarep.ToString
        Return cadena_devolver
    End Function


    <WebMethod>
    Public Shared Function getSB(ByVal Desde As String, ByVal Hasta As String) As String
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim comando As New MySqlCommand
        FDesde = Desde
        FHasta = Hasta
        comando.CommandText = "select Banco, COUNT(Banco) as total from vw_scrapper_online where STR_TO_DATE(date_format(vw_scrapper_online.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= @inicio and STR_TO_DATE(date_format(vw_scrapper_online.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= @final and LOWER(EnvioSendinblue)='false' GROUP BY Banco"
        comando.Parameters.AddWithValue("@inicio", FDesde)
        comando.Parameters.AddWithValue("@final", FHasta)

        comando.Connection = myConnection
        Dim da As New MySqlDataAdapter
        Dim ds As New DataSet
        amexrep = "0"
        bacnomerrep = "0"
        banamexrep = "0"
        hsbcrep = "0"
        scotiarep = "0"

        pamexrep = "0 %"
        pbacnomerrep = "0 %"
        pbanamexrep = "0 %"
        phsbcrep = "0 %"
        pscotiarep = "0 %"

        Try
            comando.Connection.Open()

            da.SelectCommand = comando
            da.Fill(ds, "duplicadas")
            comando.Connection.Close()

            For Each fila As DataRow In ds.Tables("duplicadas").Rows
                'porcentaje rechazadas
                Select Case fila("banco")
                    Case "AMEX"
                        amexrep = fila("total").ToString
                        pamexrep = Math.Round(((Integer.Parse(fila("total")) / Integer.Parse(TotalAmex.Replace(",", ""))) * 100)).ToString + " %"
                        If pamexrep.Contains("NaN") Then
                            pamexrep = "0 %"
                        End If
                    Case "CITIBANAMEX"
                        banamexrep = fila("total").ToString
                        pbanamexrep = Math.Round(((Integer.Parse(fila("total")) / Integer.Parse(TotalBanamex.Replace(",", ""))) * 100)).ToString + " %"
                        If pbanamexrep.Contains("NaN") Then
                            pbanamexrep = "0 %"
                        End If
                    Case "SCOTIABANK"
                        scotiarep = fila("total").ToString
                        pscotiarep = Math.Round(((Integer.Parse(fila("total")) / Integer.Parse(TotalScotia.Replace(",", ""))) * 100)).ToString + " %"
                        If pscotiarep.Contains("NaN") Then
                            pscotiarep = "0 %"
                        End If
                    Case "HSBC"
                        hsbcrep = fila("total").ToString
                        phsbcrep = Math.Round(((Integer.Parse(fila("total")) / Integer.Parse(TotalHsbc.Replace(",", ""))) * 100)).ToString + " %"
                        If phsbcrep.Contains("NaN") Then
                            phsbcrep = "0 %"
                        End If
                    Case "BANCOMER"
                        bacnomerrep = fila("total").ToString
                        pbacnomerrep = Math.Round(((Integer.Parse(fila("total")) / Integer.Parse(TotalBancomer.Replace(",", ""))) * 100)).ToString + " %"
                        If pbacnomerrep.Contains("NaN") Then
                            pbacnomerrep = "0 %"
                        End If

                End Select

            Next
        Catch ex As Exception
            If comando.Connection.State = ConnectionState.Open Then
                comando.Connection.Close()
            End If
        End Try
        Dim cadena_devolver = amexrep.ToString + "_" + bacnomerrep.ToString + "_" + banamexrep.ToString + "_" + hsbcrep.ToString + "_" + scotiarep.ToString + "&" + pamexrep.ToString + "_" + pbacnomerrep.ToString + "_" + pbanamexrep.ToString + "_" + phsbcrep.ToString + "_" + pscotiarep.ToString
        Return cadena_devolver
    End Function

    <WebMethod>
    Public Shared Function TotalScotiaNoProcesadas_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(LogScrapper.idTicket) FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'SCOTIABANK' AND  LogScrapper.Folio like '%SinFolio%' AND LogScrapper.respuesta LIKE '%SinRespuesta%' AND " &
            "LogScrapper.estatus LIKE '%SinProceso%'AND LogScrapper.comentario LIKE '%SinComentario%' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(LogScrapper.fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM log_scrapper_online AS LogScrapper " &
            "WHERE LogScrapper.Banco = 'SCOTIABANK' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(date_format(fechainicio, '%d/%m/%Y'),'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalScotiaNoProcesadas = dr(0)
                TotalScotiaNoProcesadas = FormatNumber(TotalScotiaNoProcesadas, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        'Return FormatNumber(TotalScotiaNoProcesadas, 0)
        pscotianp = Math.Round(Integer.Parse(TotalScotiaNoProcesadas.Replace(",", "")) / Integer.Parse(TotalScotia.Replace(",", "")) * 100, 2).ToString + " %"
        If pscotianp.Contains("NaN") Then
            pscotianp = "0 %"
        End If
        Return TotalScotiaNoProcesadas.ToString + "_" + pscotianp
    End Function
    <WebMethod>
    Public Shared Function RealLeads_(ByVal Desde As String, ByVal Hasta As String) As String
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT	COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                RealLeads = dr(0)
                RealLeads = FormatNumber(RealLeads, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(RealLeads, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineApplyUsers_(ByVal Desde As String, ByVal Hasta As String) As String
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;Connection Timeout=1000")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT	COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineApplyUsers = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineApplyUsers, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAuthenticatedUsers_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT	COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDLeads.AuthenticationStatus in('EXCLUSIÓN','AUTENTICADO') AND CuboBDLeads.AmexShortLead = '' AND CuboBDLeads.AuthenticationDate >= '" & Desde & "'  AND CuboBDLeads.AuthenticationDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAuthenticatedUsers = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAuthenticatedUsers, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAutenticated600_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT	COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND CuboBDLeads.AuthenticationDate >= '" & Desde & "' AND " &
            "CuboBDLeads.AuthenticationStatus in('AUTENTICADO') AND CuboBDLeads.AmexShortLead = '' AND CuboBDLeads.result = 'VENTA' AND CuboBDLeads.AuthenticationDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAutenticated600 = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAutenticated600, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsCreated_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDLeads.AuthenticationStatus in('AUTENTICADO') AND CuboBDLeads.AmexShortLead = '' AND CuboBDLeads.result = 'VENTA' AND CuboBDLeads.AuthenticationDate >= '" & Desde & "' AND CuboBDLeads.AuthenticationDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsCreated = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsCreated, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsSubmitted_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsSubmitted = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsSubmitted, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsSubmittedAmex_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult = 'SOLICITADA' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'AMEX' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsSubmittedAmex = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsSubmittedAmex, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsSubmittedScotia_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'SCOTIA' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsSubmittedScotia = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsSubmittedScotia, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsSubmittedCiti_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'CITI' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsSubmittedCiti = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsSubmittedCiti, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsSubmittedHSBC_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'HSBC' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsSubmittedHSBC = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsSubmittedHSBC, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsApprovedScotia_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE " &
            "CuboBDApps.ApplyResult = 'APROBADA' " &
            "AND CuboBDApps.Bank = 'SCOTIA' " &
            "AND CuboBDLeads.Duplicity = 'NO' " &
            "AND CuboBDLeads.Test = 'NO' " &
            "AND CuboBDLeads.Vertical = 'CREDIT CARDS' " &
            "AND CuboBDLeads.Groups In ('AUTENTICACION','DATA INPUT') " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsApprovedScotia = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsApprovedScotia, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsApprovedCiti_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE " &
            "CuboBDApps.ApplyResult = 'APROBADA' " &
            "AND CuboBDApps.Bank = 'CITI' " &
            "AND CuboBDLeads.Duplicity = 'NO' " &
            "AND CuboBDLeads.Test = 'NO' " &
            "AND CuboBDLeads.Vertical = 'CREDIT CARDS' " &
            "AND CuboBDLeads.Groups In ('AUTENTICACION','DATA INPUT') " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsApprovedCiti = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsApprovedCiti, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsApprovedHSBC_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE " &
            "CuboBDApps.ApplyResult = 'APROBADA' " &
            "AND CuboBDApps.Bank = 'HSBC' " &
            "AND CuboBDLeads.Duplicity = 'NO' " &
            "AND CuboBDLeads.Test = 'NO' " &
            "AND CuboBDLeads.Vertical = 'CREDIT CARDS' " &
            "AND CuboBDLeads.Groups In ('AUTENTICACION','DATA INPUT') " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsApprovedHSBC = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsApprovedHSBC, 0)
    End Function
    <WebMethod>
    Public Shared Function AmexShortLeads_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(*) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            " WHERE " &
            "CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.AmexShortLead = 'TRUE' " &
            "AND CuboBDLeads.`Fecha Amex Solicitud` >= '" & Desde & "' " &
            "AND CuboBDLeads.`Fecha Amex Solicitud` <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                AmexShortLeads = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(AmexShortLeads, 0)
    End Function
    <WebMethod>
    Public Shared Function LeadsForOperations_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; SELECT COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND CuboBDLeads.AmexShortLead = '' " &
        "AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                LeadsForOperations = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(LeadsForOperations, 0)
    End Function
    <WebMethod>
    Public Shared Function Campaings_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; SELECT COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND CuboBDLeads.AmexShortLead = '' " &
        "AND CuboBDLeads.campaign in ('15-H','15-SH','715-H') AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                Campaings = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(Campaings, 0)
    End Function
    <WebMethod>
    Public Shared Function SeguimientoNoContactado_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
                    "SELECT COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
                    "WHERE CuboBDLeads.Duplicity = 'NO' AND " &
                    "CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
                    "AND CuboBDLeads.campaign in ('15-H','15-SH','715-H') AND " &
                    "CuboBDLeads.result in ('SEGUIMIENTO', 'SIN ATENDER') AND " &
                    "CuboBDLeads.AmexShortLead = '' " &
                    "AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
                    "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                SeguimientoNoContactado = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(SeguimientoNoContactado, 0)
    End Function
    <WebMethod>
    Public Shared Function Rechazos_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "INNER JOIN DatZendeskTicketsMetrics ON CuboBDLeads.idTicket = DatZendeskTicketsMetrics.TicketId " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
            "AND CuboBDLeads.campaign IN ('15-H','15-SH','715-H') AND CuboBDLeads.result = 'RECHAZO CC' " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND STR_TO_DATE(DatZendeskTicketsMetrics.AssignedAt,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(DatZendeskTicketsMetrics.AssignedAt,'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                Rechazos = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(Rechazos, 0)
    End Function
    <WebMethod>
    Public Shared Function VentaUsuario_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT	COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "INNER JOIN DatZendeskTicketsMetrics ON CuboBDLeads.idTicket = DatZendeskTicketsMetrics.TicketId " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
            "AND CuboBDLeads.campaign in ('15-H','15-SH','715-H') AND CuboBDLeads.result = 'VENTA' " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND STR_TO_DATE(DatZendeskTicketsMetrics.AssignedAt,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(DatZendeskTicketsMetrics.AssignedAt,'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                VentaUsuario = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(VentaUsuario, 0)
    End Function
    <WebMethod>
    Public Shared Function OfflineAppsSubmitted_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OfflineAppsSubmitted = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OfflineAppsSubmitted, 0)
    End Function
    <WebMethod>
    Public Shared Function OfflineAppsSubmittedAmex_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'AMEX' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult = 'SOLICITADA' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OfflineAppsSubmittedAmex = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OfflineAppsSubmittedAmex, 0)
    End Function
    <WebMethod>
    Public Shared Function OfflineAppsSubmittedScotia_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'SCOTIA' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OfflineAppsSubmittedScotia = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OfflineAppsSubmittedScotia, 0)
    End Function
    <WebMethod>
    Public Shared Function OfflineAppsSubmittedHSBC_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'HSBC' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OfflineAppsSubmittedHSBC = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OfflineAppsSubmittedHSBC, 0)
    End Function
    <WebMethod>
    Public Shared Function OfflineAppsApprovedScotia_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'SCOTIA' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult = 'APROBADA' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 1000
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OfflineAppsApprovedScotia = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OfflineAppsApprovedScotia, 0)
    End Function
    <WebMethod>
    Public Shared Function OfflineAppsApprovedHSBC_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'HSBC' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult = 'APROBADA' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 1000
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OfflineAppsApprovedHSBC = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OfflineAppsApprovedHSBC, 0)
    End Function
    <WebMethod>
    Public Shared Function CreateCSVfile(ByVal Tipo As String)
        Dim strFilePath As String = "TotalLeads" & Date.Now.Ticks & ".xlsx"
        Dim Query As String = Nothing

        Select Case Tipo
            Case "TotalLeads"
                strFilePath = "TotalLeads" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT * FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Vertical = 'CREDIT CARDS' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "'; "

            Case "RealLeads"
                strFilePath = "RealLeads" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT	* FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "'; "

            Case "OnlineApplyUsers"
                strFilePath = "OnlineApplyUsers" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT	* FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "'; "

            Case "OnlineAuthenticatedUsers"
                strFilePath = "OnlineAuthenticatedUsers" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT	* FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDLeads.AuthenticationStatus in('EXCLUSIÓN','AUTENTICADO') AND CuboBDLeads.AmexShortLead = '' AND CuboBDLeads.AuthenticationDate >= '" & FDesde & "'  AND CuboBDLeads.AuthenticationDate <= '" & FHasta & "'; "

            Case "OnlineAuthenticated600"
                strFilePath = "OnlineAuthenticated600" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT	* FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND CuboBDLeads.AuthenticationDate >= '" & FDesde & "' AND " &
            "CuboBDLeads.AuthenticationStatus in('AUTENTICADO') AND CuboBDLeads.AmexShortLead = '' AND CuboBDLeads.result = 'VENTA' AND CuboBDLeads.AuthenticationDate <= '" & FHasta & "'; "

            Case "OnlineAppsCreated"
                strFilePath = "OnlineAppsCreated" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDLeads.AuthenticationStatus in('AUTENTICADO') AND CuboBDLeads.AmexShortLead = '' AND CuboBDLeads.result = 'VENTA' AND CuboBDLeads.AuthenticationDate >= '" & FDesde & "' AND CuboBDLeads.AuthenticationDate <= '" & FHasta & "'; "

            Case "OnlineAppsSubmitted"
                strFilePath = "OnlineAppsSubmitted" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OnlineAppsSubmittedAmex"
                strFilePath = "OnlineAppsSubmittedAmex" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'AMEX' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OnlineAppsSubmittedScotia"
                strFilePath = "OnlineAppsSubmittedScotia" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'SCOTIA' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OnlineAppsSubmittedCiti"
                strFilePath = "OnlineAppsSubmittedCiti" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'CITI' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OnlineAppsSubmittedHSBC"
                strFilePath = "OnlineAppsSubmittedHSBC" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'HSBC' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OnlineAppsApprovedScotia"
                strFilePath = "OnlineAppsApprovedScotia" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE " &
            "CuboBDApps.ApplyResult = 'APROBADA' " &
            "AND CuboBDApps.Bank = 'SCOTIA' " &
            "AND CuboBDLeads.Duplicity = 'NO' " &
            "AND CuboBDLeads.Test = 'NO' " &
            "AND CuboBDLeads.Vertical = 'CREDIT CARDS' " &
            "AND CuboBDLeads.Groups In ('AUTENTICACION','DATA INPUT') " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OnlineAppsApprovedCiti"
                strFilePath = "OnlineAppsApprovedCiti" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE " &
            "CuboBDApps.ApplyResult = 'APROBADA' " &
            "AND CuboBDApps.Bank = 'CITI' " &
            "AND CuboBDLeads.Duplicity = 'NO' " &
            "AND CuboBDLeads.Test = 'NO' " &
            "AND CuboBDLeads.Vertical = 'CREDIT CARDS' " &
            "AND CuboBDLeads.Groups In ('AUTENTICACION','DATA INPUT') " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OnlineAppsApprovedHSBC"
                strFilePath = "OnlineAppsApprovedHSBC" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE " &
            "CuboBDApps.ApplyResult = 'APROBADA' " &
            "AND CuboBDApps.Bank = 'HSBC' " &
            "AND CuboBDLeads.Duplicity = 'NO' " &
            "AND CuboBDLeads.Test = 'NO' " &
            "AND CuboBDLeads.Vertical = 'CREDIT CARDS' " &
            "AND CuboBDLeads.Groups In ('AUTENTICACION','DATA INPUT') " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "AmexShortLeads"
                strFilePath = "AmexShortLeads" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT * FROM CuboBDLeads_tbl AS CuboBDLeads " &
            " WHERE " &
            "CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.AmexShortLead = 'TRUE' " &
            "AND CuboBDLeads.`Fecha Amex Solicitud` >= '" & FDesde & "' " &
            "AND CuboBDLeads.`Fecha Amex Solicitud` <= '" & FHasta & "'; "

            Case "LeadsForOperations"
                strFilePath = "LeadsForOperations" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT * FROM CuboBDLeads WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
        "AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "';"

            Case "Campaings"
                strFilePath = "Campaings" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT * FROM CuboBDLeads WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
        "AND CuboBDLeads.campaign in ('15-H','15-SH','715-H') AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "';"

            Case "SeguimientoNoContactado"
                strFilePath = "SeguimientoNoContactado" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT * FROM CuboBDLeads " &
                    "WHERE CuboBDLeads.Duplicity = 'NO' AND " &
                    "CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
                    "AND CuboBDLeads.campaign in ('15-H','15-SH','715-H') AND " &
                    "CuboBDLeads.result in ('SEGUIMIENTO', 'SIN ATENDER') AND " &
                    "CuboBDLeads.AmexShortLead = '' " &
                    "AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "'; "

            Case "Rechazos"
                strFilePath = "Rechazos" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT * FROM CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
            "AND CuboBDLeads.campaign IN ('15-H','15-SH','715-H') AND CuboBDLeads.result = 'RECHAZO CC' " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "'; "

            Case "VentaUsuario"
                strFilePath = "VentaUsuario" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT	* FROM CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
            "AND CuboBDLeads.campaign in ('15-H','15-SH','715-H') AND CuboBDLeads.result = 'VENTA' " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "';"

            Case "OfflineAppsSubmitted"
                strFilePath = "OfflineAppsSubmitted" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OfflineAppsSubmittedAmex"
                strFilePath = "OfflineAppsSubmittedAmex" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'AMEX' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult = 'SOLICITADA' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OfflineAppsSubmittedScotia"
                strFilePath = "OfflineAppsSubmittedScotia" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'SCOTIA' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OfflineAppsSubmittedHSBC"
                strFilePath = "OfflineAppsSubmittedHSBC" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'HSBC' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OfflineAppsApprovedScotia"
                strFilePath = "OfflineAppsApprovedScotia" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'SCOTIA' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult = 'APROBADA' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OfflineAppsApprovedHSBC"
                strFilePath = "OfflineAppsApprovedHSBC" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'HSBC' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult = 'APROBADA' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

        End Select
        'Query = TotalLeadsQry
        Dim dtable As New DataTable("Export")
        Dim connectMySQL As String = "Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;"
        Using cn1 As MySqlConnection = New MySqlConnection(connectMySQL)
            cn1.Open()
            Dim cmdSQLite As MySqlCommand = cn1.CreateCommand()
            cmdSQLite.CommandTimeout = 300
            Dim da As New MySqlDataAdapter(cmdSQLite)
            With cmdSQLite
                .CommandType = CommandType.Text
                .CommandText = Query
            End With
            'Dim oDataTable As New DataTable("TMG")
            Dim oAdapter As New MySqlDataAdapter(cmdSQLite)
            Try
                oAdapter.Fill(dtable)
                cn1.Close()
            Catch ex As Exception
                cn1.Close()
                Console.Write(ex.Message)
            End Try
        End Using

        Dim wb = New ClosedXML.Excel.XLWorkbook
        wb.Worksheets.Add(dtable)
        wb.SaveAs("C:\Users\Edson\Documents\CoruAnalytics_git\TempFolder\" & strFilePath)
        'wb.SaveAs("W:\CoruAnalytics\TempFolder\" & strFilePath)
        ReleaseObject(wb)
        GC.Collect()
        'Return "http://localhost:57075/TempFolder/" & strFilePath
        Return "http://analytics.coru.com/TempFolder/" & strFilePath
    End Function
    Private Sub ExportToExcel(ByVal dtTemp As DataTable, ByVal filepath As String)
        Dim strFileName As String = filepath
        Dim _excel As New Excel.Application
        Dim wBook As Excel.Workbook
        Dim wSheet As Excel.Worksheet

        wBook = _excel.Workbooks.Add()
        wSheet = wBook.ActiveSheet()

        Dim dt As System.Data.DataTable = dtTemp
        Dim dc As System.Data.DataColumn
        Dim dr As System.Data.DataRow
        Dim colIndex As Integer = 0
        Dim rowIndex As Integer = 0
        'If CheckBox1.Checked Then
        For Each dc In dt.Columns
            colIndex = colIndex + 1
            wSheet.Cells(1, colIndex) = dc.ColumnName
        Next
        'End If
        For Each dr In dt.Rows
            rowIndex = rowIndex + 1
            colIndex = 0
            For Each dc In dt.Columns
                colIndex = colIndex + 1
                wSheet.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)
            Next
        Next
        wSheet.Columns.AutoFit()
        wBook.SaveAs(strFileName)

        ReleaseObject(wSheet)
        wBook.Close(False)
        ReleaseObject(wBook)
        _excel.Quit()
        ReleaseObject(_excel)
        GC.Collect()

        'MessageBox.Show("File Export Successfully!")
    End Sub
    Private Shared Sub ReleaseObject(ByVal o As Object)
        Try
            While (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0)
            End While
        Catch
        Finally
            o = Nothing
        End Try
    End Sub
    'Method to convert an column number to column name
    Private Shared Function ConvertToLetter(ByVal num As Integer) As String
        num = num - 1
        If num < 0 Or num >= 27 * 26 Then
            ConvertToLetter = "-"
        Else
            If num < 26 Then
                ConvertToLetter = Chr(num + 65)
            Else
                ConvertToLetter = Chr(num \ 26 + 64) + Chr(num Mod 26 + 65)
            End If
        End If
    End Function

    'Method convert datacolumn to array 
    Public Shared Function ToArray(ByVal dr As DataTable) As String()
        Dim ary() As String = Array.ConvertAll(Of DataRow, String)(dr.Select(), AddressOf DataRowToString)
        Return ary
    End Function

    Public Shared Function DataRowToString(ByVal dr As DataRow) As String
        Return dr(strCommonColumnName).ToString()
    End Function

    'Method convert Array to string Public 
    Public Shared Function AryToString(ByVal ary As String()) As String
        Return String.Join(vbNewLine, ToArray(dTable))
    End Function
End Class
