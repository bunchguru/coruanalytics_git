﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DashboardScrapper.aspx.vb" Inherits="DashboardScrapper" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <%--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous" />--%>
    <link href="fontawesome/css/all.css" rel="stylesheet" />
    <%--<link href="https://pro.fontawesome.com/releases/v5.1.0/css/all.css" rel="stylesheet" />--%>
    <style>
        .table thead {
            text-align: center;
            vertical-align: middle;
        }

            .table thead tr th {
                text-align: center;
                vertical-align: middle;
            }

        .table tr th {
            text-align: center;
        }

        .loader {
            border: 5px solid #f3f3f3;
            border-radius: 50%;
            border-top: 5px solid #073763;
            width: 25px;
            height: 25px;
            -webkit-animation: spin 1s linear infinite; /* Safari */
            animation: spin 1s linear infinite;
        }
        .table td div {
            margin:auto
        }
        .table th div {
            margin:auto
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
    <script>
        function GetCSV(tipo) {
            $.ajax({
                type: "POST", // Tipo de llamada
                beforeSend: function () {
                    $("#Down"+ tipo).html("<div id='download" + tipo + "'></div>");
                    $("#download" + tipo).addClass("loader");
                    console.log("Enviando petición de descarga "+ tipo);
                },
                async: true,
                url: "Dashboard.aspx/CreateCSVfile",
                data: '{Tipo: "' + tipo + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var datos = '<i class="fal fa-arrow-alt-to-bottom fa-2x" onclick="GetCSV('+ "'" + tipo + "'" +');">';
                    console.log(datos);
                    $("#Down" + tipo).html(datos);
                    window.open(result.d, "_blank");
                    //alert("Prueba exitosa");

                },
                
            });
        }
    </script>
</head>
<body>
    <div class="container">
        <form id="form1" runat="server">

            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    <div class="text-center">
                        <h1>Analytics Scrapper online</h1>
                    </div>
                </div>
                <div class="col-4"></div>
            </div>
            <div class="row">
                <div class="col-9"></div>
                <div class="col-3">
                    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                        <i class="fa fa-calendar"></i>
                        &nbsp;
                <span></span>
                        <i class="fa fa-caret-down"></i>
                    </div>
                </div>
            </div>
            <br />
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead style="background-color: #073763; color: #FFF;">
                        <tr>
                            <th scope="col" style="background-color: #fff!important; color: #000000;">Scrappers</th>
                            <th scope="col">Amex</th>
                            <th scope="col">Bancomer</th>
                            <th scope="col">Banamex</th>
                            <th scope="col">Hsbc</th>
                            <th scope="col">ScotiaBank</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row" style="background-color: #073763; color: #FFF;" >Total de scrappers</th>
                            <td scope="row" id="totalAmex" style="text-align:center;"><%=TotalAmex %></td>
                            <td scope="row" id="totalBancomer" style="text-align:center;"><%=TotalBancomer %></td>
                            <td scope="row" id="totalBanamex" style="text-align:center;"><%=TotalBanamex %></td>
                            <td scope="row" id="totalHsbc" style="text-align:center;"><%=TotalHsbc %></td>
                            <td scope="row" id="totalScotia" style="text-align:center;"><%=TotalScotia %></td>                            
                        </tr>
                        <tr>
                            <th scope="row" style="background-color: #073763; color: #FFF;">Aprobadas</th>
                            <td scope="row" id="totalAprobadasAmex" style="text-align:center;"><%=TotalAmexAprobadas %></td>
                            <td scope="row" id="totalAprobadasBancomer" style="text-align:center;"><%=TotalBancomerAprobadas %></td>
                            <td scope="row" id="totalAprobadasBanamex" style="text-align:center;"><%=TotalBanamexAprobadas %></td>
                            <td scope="row" id="totalAprobadasHsbc" style="text-align:center;"><%=TotalHsbcAprobadas %></td>
                            <td scope="row" id="totalAprobadasScotia" style="text-align:center;"><%=TotalScotiaAprobadas %></td>                            
                        </tr>

                        <tr>
                            <th scope="row" style="background-color: #073763; color: #FFF;">% Aprobadas</th>
                            <td scope="row" id="pamexaprobada" style="text-align:center;"><%= pamexaprobada %></td>
                            <td scope="row" id="pbacnomeraprobada" style="text-align:center;"><%=pbacnomeraprobada %></td>
                            <td scope="row" id="pbanamexaprobada" style="text-align:center;"><%=pbanamexaprobada %></td>
                            <td scope="row" id="phsbcaprobada" style="text-align:center;"><%=phsbcaprobada %></td>
                            <td scope="row" id="pscotiaaprobada" style="text-align:center;"><%=pscotiaaprobada %></td>                            
                        </tr>

                        <tr>
                            <th scope="row" style="background-color: #073763; color: #FFF;">Rechazadas</th>                             
                            <td scope="row" id="totalRechazadasAmex" style="text-align:center;"><%=TotalAmexRechazadas %></td>
                            <td scope="row" id="totalRechazadasBancomer" style="text-align:center;"><%=TotalBancomerRechazadas %></td>
                            <td scope="row" id="totalRechazadasBanamex" style="text-align:center;"><%=TotalBanamexRechazadas %></td>
                            <td scope="row" id="totalRechazadasHsbc" style="text-align:center;"><%=TotalHsbcRechazadas %></td>
                            <td scope="row" id="totalRechazadasScotia" style="text-align:center;"><%=TotalScotiaRechazadas %></td>
                           
                        </tr>

                        <tr>
                            <th scope="row" style="background-color: #073763; color: #FFF;">% Rechazadas</th>                             
                            <td scope="row" id="pamexrechazada" style="text-align:center;"><%=pamexrechazada %></td>
                            <td scope="row" id="pbacnomerrechazada" style="text-align:center;"><%=pbacnomerrechazada %></td>
                            <td scope="row" id="pbanamexrechazada" style="text-align:center;"><%=pbanamexrechazada %></td>
                            <td scope="row" id="phsbcrechazada" style="text-align:center;"><%=phsbcrechazada %></td>
                            <td scope="row" id="pscotiarechazada" style="text-align:center;"><%=pscotiarechazada %></td>
                           
                        </tr>
                        
                        <tr>
                            <th scope="row" style="background-color: #073763; color: #FFF;">Error</th>
                            <td scope="row" id="totalErrorAmex" style="text-align:center;"><%=TotalAmexError %></td>
                            <td scope="row" id="totalErrorBancomer" style="text-align:center;"><%=TotalBancomerError %></td>
                            <td scope="row" id="totalErrorBanamex" style="text-align:center;"><%=TotalBanamexError %></td>
                            <td scope="row" id="totalErrorHsbc" style="text-align:center;"><%=TotalHsbcError %></td>
                            <td scope="row" id="totalErrorScotia" style="text-align:center;"><%=TotalScotiaError %></td>                           
                        </tr>

                        <tr>
                            <th scope="row" style="background-color: #073763; color: #FFF;">% Error</th>
                            <td scope="row" id="pamexerror" style="text-align:center;"><%=pamexerror %></td>
                            <td scope="row" id="pbacnomererror" style="text-align:center;"><%=pbacnomererror %></td>
                            <td scope="row" id="pbanamexerror" style="text-align:center;"><%=pbanamexerror %></td>
                            <td scope="row" id="phsbcerror" style="text-align:center;"><%=phsbcerror %></td>
                            <td scope="row" id="pscotiaerror" style="text-align:center;"><%=pscotiaerror %></td>                           
                        </tr>

                        <tr>
                            <th scope="row" style="background-color: #073763; color: #FFF;">No procesadas</th>
                            <td scope="row" id="totalNoProcesadasAmex" style="text-align:center;"><%=TotalAmexNoProcesadas %></td>
                            <td scope="row" id="totalNoProcesadasBancomer" style="text-align:center;"><%=TotalBancomerNoProcesadas %></td>
                            <td scope="row" id="totalNoProcesadasBanamex" style="text-align:center;"><%=TotalBanamexNoProcesadas %></td>
                            <td scope="row" id="totalNoProcesadasHsbc" style="text-align:center;"><%=TotalHsbcNoProcesadas %></td>
                            <td scope="row" id="totalNoProcesadasScotia" style="text-align:center;"><%=TotalScotiaNoProcesadas %></td>                           
                        </tr>

                        <tr>
                            <th scope="row" style="background-color: #073763; color: #FFF;">% No procesadas</th>
                            <td scope="row" id="pamexnp" style="text-align:center;"><%=pamexnp %></td>
                            <td scope="row" id="pbacnomernp" style="text-align:center;"><%=pbacnomernp %></td>
                            <td scope="row" id="pbanamexnp" style="text-align:center;"><%=pbanamexnp %></td>
                            <td scope="row" id="phsbcnp" style="text-align:center;"><%=phsbcnp %></td>
                            <td scope="row" id="pscotianp" style="text-align:center;"><%=pscotianp %></td>                           
                        </tr>
                        
                    </tbody>
                </table>
                <br />
                <br />
                </div>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead style="background-color: #073763; color: #FFF;">
                        <tr>
                            <th scope="col" style="background-color: #fff!important; color: #000000;"></th>
                            <th scope="col">Amex</th>
                            <th scope="col">Bancomer</th>
                            <th scope="col">Banamex</th>
                            <th scope="col">Hsbc</th>
                            <th scope="col">ScotiaBank</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row" style="background-color: #073763; color: #FFF;">Repetidos</th>
                            <td scope="row" id="amexrep" style="text-align:center;"><%=amexrep %></td>
                            <td scope="row" id="bacnomerrep" style="text-align:center;"><%=bacnomerrep %></td>
                            <td scope="row" id="banamexrep" style="text-align:center;"><%=banamexrep %></td>
                            <td scope="row" id="hsbcrep" style="text-align:center;"><%=hsbcrep %></td>
                            <td scope="row" id="scotiarep" style="text-align:center;"><%=scotiarep %></td>                           
                        </tr>
                        <tr>
                            <th scope="row" style="background-color: #073763; color: #FFF;">% Repetidos</th>
                            <td scope="row" id="pamexrep" style="text-align:center;"><%=pamexrep %></td>
                            <td scope="row" id="pbacnomerrep" style="text-align:center;"><%=pbacnomerrep %></td>
                            <td scope="row" id="pbanamexrep" style="text-align:center;"><%=pbanamexrep %></td>
                            <td scope="row" id="phsbcrep" style="text-align:center;"><%=phsbcrep %></td>
                            <td scope="row" id="pscotiarep" style="text-align:center;"><%=pscotiarep %></td>                           
                        </tr>
                    </tbody>

                </table>

            </div>            
            <br />
            <br />
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead style="background-color: #073763; color: #FFF;">
                        <tr>
                            <th scope="col" style="background-color: #fff!important; color: #000000;"></th>
                            <th scope="col">Amex</th>
                            <th scope="col">Bancomer</th>
                            <th scope="col">Banamex</th>
                            <th scope="col">Hsbc</th>
                            <th scope="col">ScotiaBank</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row" style="background-color: #073763; color: #FFF;">Etiqueta mal enviada Sendinblue</th>
                            <td scope="row" id="amexsb" style="text-align:center;"><%=amexrep %></td>
                            <td scope="row" id="bacnomersb" style="text-align:center;"><%=bacnomerrep %></td>
                            <td scope="row" id="banamexsb" style="text-align:center;"><%=banamexrep %></td>
                            <td scope="row" id="hsbcsb" style="text-align:center;"><%=hsbcrep %></td>
                            <td scope="row" id="scotiasb" style="text-align:center;"><%=scotiarep %></td>                           
                        </tr>
                        <tr>
                            <th scope="row" style="background-color: #073763; color: #FFF;">% Etiqueta mal enviada Sendinblue</th>
                            <td scope="row" id="pamexsb" style="text-align:center;"><%=pamexrep %></td>
                            <td scope="row" id="pbacnomersb" style="text-align:center;"><%=pbacnomerrep %></td>
                            <td scope="row" id="pbanamexsb" style="text-align:center;"><%=pbanamexrep %></td>
                            <td scope="row" id="phsbcsb" style="text-align:center;"><%=phsbcrep %></td>
                            <td scope="row" id="pscotiasb" style="text-align:center;"><%=pscotiarep %></td>                           
                        </tr>
                    </tbody>

                </table>

            </div>            

        </form>
    </div>
</body>
</html>

<script type="text/javascript">
    var globalFinDate ="";
    var globalInicioDate = "";
    $(function () {

        var start = moment().startOf('month');
        var end = moment().endOf('month');
        globalFinDate = end.format('YYYY-MM-DD');
        globalInicioDate = start.format('YYYY-MM-DD')
        function cb(start, end) {
            $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        }

        $(function () {
            $('#reportrange').daterangepicker({
                "locale": {
                    "format": "DD-MM-YYYY",
                    "separator": " - ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },
                "startDate": start,
                "endDate": end,
                "opens": "left"
            }, cb);
        });

        cb(start, end);
        //numero de scrapper ejecutados omar
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalAmex').html('<div id="loader1"></div>');
                $('#loader1').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalAmex_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalAmex').html(result.d);
            }
        });

        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalBancomer').html('<div id="loader2"></div>');
                $('#loader2').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBancomer_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalBancomer').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalBanamex').html('<div id="loader3"></div>');
                $('#loader3').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBanamex_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalBanamex').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalHsbc').html('<div id="loader4"></div>');
                $('#loader4').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalHsbc_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalHsbc').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalScotia').html('<div id="loader5"></div>');
                $('#loader5').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalScotia_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalScotia').html(result.d);
            }
        });
        //aprobadas
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalAprobadasAmex').html('<div id="loader6"></div>');
                $('#pamexaprobada').html('<div id="loader26"></div>');
                $('#loader6').addClass('loader');
                $('#loader26').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalAmexAprobadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                valores = result.d.split("_")

                $('#totalAprobadasAmex').html(valores[0]);
                $('#pamexaprobada').html(valores[1]);
            }
        });

        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalAprobadasBancomer').html('<div id="loader7"></div>');
                $('#pbacnomeraprobada').html('<div id="loader27"></div>');
                $('#loader7').addClass('loader');
                $('#loader27').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBancomerAprobadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                valores = result.d.split("_")
                $('#totalAprobadasBancomer').html(valores[0]);
                $('#pbacnomeraprobada').html(valores[1]);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalAprobadasBanamex').html('<div id="loader8"></div>');
                $('#pbanamexaprobada').html('<div id="loader28"></div>');
                $('#loader8').addClass('loader');
                $('#loader28').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBanamexAprobadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                valores = result.d.split("_")
                $('#totalAprobadasBanamex').html(valores[0]);
                $('#pbanamexaprobada').html(valores[1]);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalAprobadasHsbc').html('<div id="loader9"></div>');
                $('#phsbcaprobada').html('<div id="loader29"></div>');
                $('#loader9').addClass('loader');
                $('#loader29').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalHsbcAprobadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);                
                valores = result.d.split("_")
                $('#totalAprobadasHsbc').html(valores[0]);
                $('#phsbcaprobada').html(valores[1]);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalAprobadasScotia').html('<div id="loader10"></div>');
                $('#pscotiaaprobada').html('<div id="loader30"></div>');
                $('#loader10').addClass('loader');
                $('#loader30').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalScotiaAprobadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);                
                valores = result.d.split("_")
                $('#totalAprobadasScotia').html(valores[0]);
                $('#pscotiaaprobada').html(valores[1]);
            }
        });

        
        
        //RECHAZADAS
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalRechazadasAmex').html('<div id="loader11"></div>');
                $('#pamexrechazada').html('<div id="loader31"></div>');
                $('#loader11').addClass('loader');
                $('#loader31').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalAmexRechazadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                //$('#totalRechazadasAmex').html(result.d);
                valores = result.d.split("_")
                $('#totalRechazadasAmex').html(valores[0]);
                $('#pamexrechazada').html(valores[1]);
            }
        });

        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalRechazadasBancomer').html('<div id="loader12"></div>');
                $('#pbacnomerrechazada').html('<div id="loader32"></div>');
                $('#loader12').addClass('loader');
                $('#loader32').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBancomerRechazadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                valores = result.d.split("_")
                $('#totalRechazadasBancomer').html(valores[0]);
                $('#pbacnomerrechazada').html(valores[1]);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalRechazadasBanamex').html('<div id="loader13"></div>');
                $('#pbanamexrechazada').html('<div id="loader33"></div>');
                $('#loader13').addClass('loader');
                $('#loader33').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBanamexRechazadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                
                valores = result.d.split("_")
                $('#totalRechazadasBanamex').html(valores[0]);
                $('#pbanamexrechazada').html(valores[1]);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalRechazadasHsbc').html('<div id="loader14"></div>');
                $('#phsbcrechazada').html('<div id="loader34"></div>');
                $('#loader14').addClass('loader');
                $('#loader34').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalHsbcRechazadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                //$('#totalRechazadasHsbc').html(result.d);
                valores = result.d.split("_")
                $('#totalRechazadasHsbc').html(valores[0]);
                $('#phsbcrechazada').html(valores[1]);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalRechazadasScotia').html('<div id="loader15"></div>');
                $('#pscotiarechazada').html('<div id="loader35"></div>');
                $('#loader15').addClass('loader');
                $('#loader35').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalScotiaRechazadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                //$('#totalRechazadasScotia').html(result.d);
                valores = result.d.split("_")
                $('#totalRechazadasScotia').html(valores[0]);
                $('#pscotiarechazada').html(valores[1]);
            }
        });
        //error        
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalErrorAmex').html('<div id="loader16"></div>');
                $('#pamexerror').html('<div id="loader36"></div>');
                $('#loader16').addClass('loader');
                $('#loader36').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalAmexError_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                
                valores = result.d.split("_")
                $('#totalErrorAmex').html(valores[0]);
                $('#pamexerror').html(valores[1]);
            }
        });

        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalErrorBancomer').html('<div id="loader17"></div>');
                $('#pbacnomererror').html('<div id="loader37"></div>');
                $('#loader17').addClass('loader');
                $('#loader37').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBancomerError_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                //$('#totalErrorBancomer').html(result.d);
                valores = result.d.split("_")
                $('#totalErrorBancomer').html(valores[0]);
                $('#pbacnomererror').html(valores[1]);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalErrorBanamex').html('<div id="loader18"></div>');
                $('#pbanamexerror').html('<div id="loader38"></div>');
                $('#loader18').addClass('loader');
                $('#loader38').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBanamexError_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                //$('#totalErrorBanamex').html(result.d);
                valores = result.d.split("_")
                $('#totalErrorBanamex').html(valores[0]);
                $('#pbanamexerror').html(valores[1]);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalErrorHsbc').html('<div id="loader19"></div>');
                $('#phsbcerror').html('<div id="loader39"></div>');
                $('#loader19').addClass('loader');
                $('#loader39').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalHsbcError_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                //$('#totalErrorHsbc').html(result.d);
                valores = result.d.split("_")
                $('#totalErrorHsbc').html(valores[0]);
                $('#phsbcerror').html(valores[1]);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalErrorScotia').html('<div id="loader20"></div>');
                $('#pscotiaerror').html('<div id="loader40"></div>');
                $('#loader20').addClass('loader');
                $('#loader40').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalScotiaError_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                //$('#totalErrorScotia').html(result.d);
                valores = result.d.split("_")
                $('#totalErrorScotia').html(valores[0]);
                $('#pscotiaerror').html(valores[1]);
            }
        });
        //noprocesadas
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalNoProcesadasAmex').html('<div id="loader21"></div>');
                $('#pamexnp').html('<div id="loader41"></div>');
                $('#loader21').addClass('loader');
                $('#loader41').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalAmexNoProcesadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                //$('#totalNoProcesadasAmex').html(result.d);
                valores = result.d.split("_");
                $('#totalNoProcesadasAmex').html(valores[0]);
                $('#pamexnp').html(valores[1]);
            }
        });

        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalNoProcesadasBancomer').html('<div id="loader22"></div>');
                $('#pbacnomernp').html('<div id="loader42"></div>');
                $('#loader22').addClass('loader');
                $('#loader42').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBancomerNoProcesadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                //$('#totalNoProcesadasBancomer').html(result.d);
                valores = result.d.split("_");
                $('#totalNoProcesadasBancomer').html(valores[0]);
                $('#pbacnomernp').html(valores[1]);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalNoProcesadasBanamex').html('<div id="loader23"></div>');
                $('#pbanamexnp').html('<div id="loader43"></div>');
                $('#loader23').addClass('loader');
                $('#loader43').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBanamexNoProcesadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                //$('#totalNoProcesadasBanamex').html(result.d);
                valores = result.d.split("_");
                $('#totalNoProcesadasBanamex').html(valores[0]);
                $('#pbanamexnp').html(valores[1]);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalNoProcesadasHsbc').html('<div id="loader24"></div>');
                $('#phsbcnp').html('<div id="loader44"></div>');
                $('#loader24').addClass('loader');
                $('#loader44').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalHsbcNoProcesadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                //$('#totalNoProcesadasHsbc').html(result.d);
                valores = result.d.split("_");
                $('#totalNoProcesadasHsbc').html(valores[0]);
                $('#phsbcnp').html(valores[1]);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalNoProcesadasScotia').html('<div id="loader25"></div>');
                $('#pscotianp').html('<div id="loader45"></div>');
                $('#loader25').addClass('loader');
                $('#loader45').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalScotiaNoProcesadas_",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                //$('#totalNoProcesadasScotia').html(result.d);
                valores = result.d.split("_");
                $('#totalNoProcesadasScotia').html(valores[0]);
                $('#pscotianp').html(valores[1]);
            }
        });
        //end omar

        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#amexrep').html('<div id="loader46"></div>');
                $('#bacnomerrep').html('<div id="loader47"></div>');
                $('#banamexrep').html('<div id="loader48"></div>');
                $('#scotiarep').html('<div id="loader49"></div>');
                $('#hsbcrep').html('<div id="loader50"></div>');
                $('#pamexrep').html('<div id="loader51"></div>');
                $('#pbacnomerrep').html('<div id="loader52"></div>');
                $('#pbanamexrep').html('<div id="loader53"></div>');
                $('#pscotiarep').html('<div id="loader54"></div>');
                $('#phsbcrep').html('<div id="loader55"></div>');



                $('#loader46').addClass('loader');
                $('#loader47').addClass('loader');
                $('#loader48').addClass('loader');
                $('#loader49').addClass('loader');
                $('#loader50').addClass('loader');
                $('#loader51').addClass('loader');
                $('#loader52').addClass('loader');
                $('#loader53').addClass('loader');
                $('#loader54').addClass('loader');
                $('#loader55').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/getduplicados",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                //$('#totalNoProcesadasScotia').html(result.d);
                valores = result.d.split("&");
                totales=valores[0].split("_")
                porcentajes = valores[1].split("_")

                $('#amexrep').html(totales[0]);
                $('#bacnomerrep').html(totales[1]);
                $('#banamexrep').html(totales[2]);
                $('#scotiarep').html(totales[4]);
                $('#hsbcrep').html(totales[3]);
                $('#pamexrep').html(porcentajes[0]);
                $('#pbacnomerrep').html(porcentajes[1]);
                $('#pbanamexrep').html(porcentajes[2]);
                $('#pscotiarep').html(porcentajes[4]);
                $('#phsbcrep').html(porcentajes[3]);
                
            }
        });

        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#amexsb').html('<div id="loader56"></div>');
                $('#bacnomersb').html('<div id="loader57"></div>');
                $('#banamexsb').html('<div id="loader58"></div>');
                $('#scotiasb').html('<div id="loader59"></div>');
                $('#hsbcsb').html('<div id="loader60"></div>');
                $('#pamexsb').html('<div id="loader61"></div>');
                $('#pbacnomersb').html('<div id="loader62"></div>');
                $('#pbanamexsb').html('<div id="loader63"></div>');
                $('#pscotiasb').html('<div id="loader64"></div>');
                $('#phsbcsb').html('<div id="loader65"></div>');



                $('#loader56').addClass('loader');
                $('#loader57').addClass('loader');
                $('#loader58').addClass('loader');
                $('#loader59').addClass('loader');
                $('#loader60').addClass('loader');
                $('#loader61').addClass('loader');
                $('#loader62').addClass('loader');
                $('#loader63').addClass('loader');
                $('#loader64').addClass('loader');
                $('#loader65').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/getSB",
            data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                //$('#totalNoProcesadasScotia').html(result.d);
                valores = result.d.split("&");
                totales=valores[0].split("_")
                porcentajes = valores[1].split("_")

                $('#amexsb').html(totales[0]);
                $('#bacnomersb').html(totales[1]);
                $('#banamexsb').html(totales[2]);
                $('#scotiasb').html(totales[4]);
                $('#hsbcsb').html(totales[3]);
                $('#pamexsb').html(porcentajes[0]);
                $('#pbacnomersb').html(porcentajes[1]);
                $('#pbanamexsb').html(porcentajes[2]);
                $('#pscotiasb').html(porcentajes[4]);
                $('#phsbcsb').html(porcentajes[3]);
                
            }
        });

        /*$("#totalAmex").click(function (event) {
            alert("ini: " + globalInicioDate + "fin: " + globalFinDate);
            $.ajax({
                type: "POST", // Tipo de llamada
                beforeSend: function () {
                    $('#totalAmex').html('<div id="loader1"></div>');
                    $('#loader1').addClass('loader');
                    console.log("Enviando petición de Total Leads");
                },
                async: true,
                url: "DashboardScrapper.aspx/TotalAmex_",
                data: '{Desde: "' + start.format('YYYY-MM-DD') + '", Hasta: "' + end.format('YYYY-MM-DD') + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    console.log(result.d);
                    $('#totalAmex').html(result.d);
                }
            });
        });*/
        
    });
    




    $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        var desde = picker.startDate.format('YYYY-MM-DD');
        var hasta = picker.endDate.format('YYYY-MM-DD');
        globalInicioDate = picker.startDate.format('YYYY-MM-DD');
        globalFinDate = picker.endDate.format('YYYY-MM-DD');


        
        //numero de scrapper ejecutados omar
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalAmex').html('<div id="loader1"></div>');
                $('#loader1').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalAmex_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalAmex').html(result.d);
            }
        });

        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalBancomer').html('<div id="loader2"></div>');
                $('#loader2').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBancomer_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalBancomer').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalBanamex').html('<div id="loader3"></div>');
                $('#loader3').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBanamex_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalBanamex').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalHsbc').html('<div id="loader4"></div>');
                $('#loader4').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalHsbc_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalHsbc').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalScotia').html('<div id="loader5"></div>');
                $('#loader5').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalScotia_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalScotia').html(result.d);
            }
        });
        //aprobadas
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalAprobadasAmex').html('<div id="loader6"></div>');
                $('#loader6').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalAmexAprobadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalAprobadasAmex').html(result.d);
            }
        });

        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalAprobadasBancomer').html('<div id="loader7"></div>');
                $('#loader7').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBancomerAprobadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalAprobadasBancomer').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalAprobadasBanamex').html('<div id="loader8"></div>');
                $('#loader8').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBanamexAprobadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalAprobadasBanamex').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalAprobadasHsbc').html('<div id="loader9"></div>');
                $('#loader9').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalHsbcAprobadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalAprobadasHsbc').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalAprobadasScotia').html('<div id="loader10"></div>');
                $('#loader10').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalScotiaAprobadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalAprobadasScotia').html(result.d);
            }
        });
        //RECHAZADAS
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalRechazadasAmex').html('<div id="loader11"></div>');
                $('#loader11').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalAmexRechazadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalRechazadasAmex').html(result.d);
            }
        });

        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalRechazadasBancomer').html('<div id="loader12"></div>');
                $('#loader12').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBancomerRechazadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalRechazadasBancomer').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalRechazadasBanamex').html('<div id="loader13"></div>');
                $('#loader13').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBanamexRechazadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalRechazadasBanamex').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalRechazadasHsbc').html('<div id="loader14"></div>');
                $('#loader14').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalHsbcRechazadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalRechazadasHsbc').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalRechazadasScotia').html('<div id="loader15"></div>');
                $('#loader15').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalScotiaRechazadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalRechazadasScotia').html(result.d);
            }
        });
        //error        
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalErrorAmex').html('<div id="loader16"></div>');
                $('#loader16').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalAmexError_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalErrorAmex').html(result.d);
            }
        });

        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalErrorBancomer').html('<div id="loader17"></div>');
                $('#loader17').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBancomerError_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalErrorBancomer').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalErrorBanamex').html('<div id="loader18"></div>');
                $('#loader18').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBanamexError_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalErrorBanamex').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalErrorHsbc').html('<div id="loader19"></div>');
                $('#loader19').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalHsbcError_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalErrorHsbc').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalErrorScotia').html('<div id="loader20"></div>');
                $('#loader20').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalScotiaError_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalErrorScotia').html(result.d);
            }
        });
        //noprocesadas
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalNoProcesadasAmex').html('<div id="loader21"></div>');
                $('#loader21').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalAmexNoProcesadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalNoProcesadasAmex').html(result.d);
            }
        });

        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalNoProcesadasBancomer').html('<div id="loader22"></div>');
                $('#loader22').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBancomerNoProcesadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalNoProcesadasBancomer').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalNoProcesadasBanamex').html('<div id="loader23"></div>');
                $('#loader23').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalBanamexNoProcesadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalNoProcesadasBanamex').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalNoProcesadasHsbc').html('<div id="loader24"></div>');
                $('#loader24').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalHsbcNoProcesadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalNoProcesadasHsbc').html(result.d);
            }
        });
        $.ajax({
            type: "POST", // Tipo de llamada
            beforeSend: function () {
                $('#totalNoProcesadasScotia').html('<div id="loader25"></div>');
                $('#loader25').addClass('loader');
                console.log("Enviando petición de Total Leads");
            },
            async: true,
            url: "DashboardScrapper.aspx/TotalScotiaNoProcesadas_",
            data: '{Desde: "' + desde + '", Hasta: "' + hasta + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                console.log(result.d);
                $('#totalNoProcesadasScotia').html(result.d);
            }
        });
        //end omar
    });
</script>