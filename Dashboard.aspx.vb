﻿
Imports System.Data
Imports System.IO
Imports System.Threading
Imports System.Web.Services
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports Newtonsoft.Json
Imports Excel = Microsoft.Office.Interop.Excel
Imports ClosedXML

Partial Class Dashboard
    Inherits System.Web.UI.Page
    Public Shared TotalLeads, RealLeads, ConversionRate, OnlineApplyUsers, OnlineAuthenticatedUsers, OnlineAutenticated, OnlineAutenticated600,
        SellabeLeadsConversionRate, OnlineAppsCreated, OnlineAppsSubmitted, OnlineAppsSubmittedAmex, OnlineAppsSubmittedScotia, OnlineAppsSubmittedCiti,
        OnlineAppsSubmittedHSBC, OnlineAppsApproved, OfflineAppsApprovedAmex, OfflineAppsApprovedScotia, OfflineAppsApprovedCiti,
        OfflineAppsApprovedHSBC, AmexShortLeads, LeadsForOperations, Campaings, SeguimientoNoContactado, Rechazos, VentaUsuario,
        VentaApps, VentaAppsAmex, VentaAppsScotia, OfflineAppsApproved, Amex, Scotia, ResuelveTuDeuda, FDesde, FHasta, OnlineAppsApprovedScotia,
        OnlineAppsApprovedCiti, OnlineAppsApprovedHSBC, OfflineAppsSubmitted, OfflineAppsSubmittedAmex, OfflineAppsSubmittedScotia,
        OfflineAppsSubmittedHSBC, Desde, Hasta As String

    Public Shared TotalLeadsQry, RealLeadsQey, strCommonColumnName As String
    Public Shared dTable As DataTable
    Public Function FirstDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Return New DateTime(sourceDate.Year, sourceDate.Month, 1)
    End Function

    'Get the last day of the month
    Public Function LastDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Dim lastDay As DateTime = New DateTime(sourceDate.Year, sourceDate.Month, 1)
        Return lastDay.AddMonths(1).AddDays(-1)
    End Function
    Private Sub Dashboard_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim Start As String = FirstDayOfMonth(Date.Now).ToString("yyyy-MM-dd")
        Dim Ends As String = LastDayOfMonth(Date.Now).ToString("yyyy-MM-dd")
    End Sub
    <WebMethod>
    Public Shared Function TotalLeads_(ByVal Desde As String, ByVal Hasta As String) As String
        FDesde = Desde
        FHasta = Hasta
        'Dim TotalLeads As String
        '"SELECT COUNT(idTicket) FROM CuboBDLeads WHERE STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " &
            "SELECT COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Vertical = 'CREDIT CARDS' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        TotalLeadsQry = "SELECT * FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Vertical = 'CREDIT CARDS' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; "

        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand

        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 600
            dr = myCommand.ExecuteReader()
            While dr.Read()
                TotalLeads = dr(0)
                TotalLeads = FormatNumber(TotalLeads, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(TotalLeads, 0)
    End Function
    <WebMethod>
    Public Shared Function RealLeads_(ByVal Desde As String, ByVal Hasta As String) As String
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT	COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                RealLeads = dr(0)
                RealLeads = FormatNumber(RealLeads, 0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(RealLeads, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineApplyUsers_(ByVal Desde As String, ByVal Hasta As String) As String
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;Connection Timeout=1000")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT	COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineApplyUsers = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineApplyUsers, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAuthenticatedUsers_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT	COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDLeads.AuthenticationStatus in('EXCLUSIÓN','AUTENTICADO') AND CuboBDLeads.AmexShortLead = '' AND CuboBDLeads.AuthenticationDate >= '" & Desde & "'  AND CuboBDLeads.AuthenticationDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAuthenticatedUsers = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAuthenticatedUsers, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAutenticated600_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT	COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND CuboBDLeads.AuthenticationDate >= '" & Desde & "' AND " &
            "CuboBDLeads.AuthenticationStatus in('AUTENTICADO') AND CuboBDLeads.AmexShortLead = '' AND CuboBDLeads.result = 'VENTA' AND CuboBDLeads.AuthenticationDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAutenticated600 = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAutenticated600, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsCreated_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDLeads.AuthenticationStatus in('AUTENTICADO') AND CuboBDLeads.AmexShortLead = '' AND CuboBDLeads.result = 'VENTA' AND CuboBDLeads.AuthenticationDate >= '" & Desde & "' AND CuboBDLeads.AuthenticationDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsCreated = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsCreated, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsSubmitted_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsSubmitted = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsSubmitted, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsSubmittedAmex_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult = 'SOLICITADA' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'AMEX' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsSubmittedAmex = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsSubmittedAmex, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsSubmittedScotia_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'SCOTIA' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsSubmittedScotia = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsSubmittedScotia, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsSubmittedCiti_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'CITI' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsSubmittedCiti = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsSubmittedCiti, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsSubmittedHSBC_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'HSBC' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsSubmittedHSBC = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsSubmittedHSBC, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsApprovedScotia_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE " &
            "CuboBDApps.ApplyResult = 'APROBADA' " &
            "AND CuboBDApps.Bank = 'SCOTIA' " &
            "AND CuboBDLeads.Duplicity = 'NO' " &
            "AND CuboBDLeads.Test = 'NO' " &
            "AND CuboBDLeads.Vertical = 'CREDIT CARDS' " &
            "AND CuboBDLeads.Groups In ('AUTENTICACION','DATA INPUT') " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsApprovedScotia = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsApprovedScotia, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsApprovedCiti_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE " &
            "CuboBDApps.ApplyResult = 'APROBADA' " &
            "AND CuboBDApps.Bank = 'CITI' " &
            "AND CuboBDLeads.Duplicity = 'NO' " &
            "AND CuboBDLeads.Test = 'NO' " &
            "AND CuboBDLeads.Vertical = 'CREDIT CARDS' " &
            "AND CuboBDLeads.Groups In ('AUTENTICACION','DATA INPUT') " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsApprovedCiti = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsApprovedCiti, 0)
    End Function
    <WebMethod>
    Public Shared Function OnlineAppsApprovedHSBC_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE " &
            "CuboBDApps.ApplyResult = 'APROBADA' " &
            "AND CuboBDApps.Bank = 'HSBC' " &
            "AND CuboBDLeads.Duplicity = 'NO' " &
            "AND CuboBDLeads.Test = 'NO' " &
            "AND CuboBDLeads.Vertical = 'CREDIT CARDS' " &
            "AND CuboBDLeads.Groups In ('AUTENTICACION','DATA INPUT') " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OnlineAppsApprovedHSBC = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OnlineAppsApprovedHSBC, 0)
    End Function
    <WebMethod>
    Public Shared Function AmexShortLeads_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(*) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            " WHERE " &
            "CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.AmexShortLead = 'TRUE' " &
            "AND CuboBDLeads.`Fecha Amex Solicitud` >= '" & Desde & "' " &
            "AND CuboBDLeads.`Fecha Amex Solicitud` <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                AmexShortLeads = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(AmexShortLeads, 0)
    End Function
    <WebMethod>
    Public Shared Function LeadsForOperations_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; SELECT COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND CuboBDLeads.AmexShortLead = '' " &
        "AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                LeadsForOperations = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(LeadsForOperations, 0)
    End Function
    <WebMethod>
    Public Shared Function Campaings_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; SELECT COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND CuboBDLeads.AmexShortLead = '' " &
        "AND CuboBDLeads.campaign in ('15-H','15-SH','715-H') AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                Campaings = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(Campaings, 0)
    End Function
    <WebMethod>
    Public Shared Function SeguimientoNoContactado_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
                    "SELECT COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
                    "WHERE CuboBDLeads.Duplicity = 'NO' AND " &
                    "CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
                    "AND CuboBDLeads.campaign in ('15-H','15-SH','715-H') AND " &
                    "CuboBDLeads.result in ('SEGUIMIENTO', 'SIN ATENDER') AND " &
                    "CuboBDLeads.AmexShortLead = '' " &
                    "AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & Hasta & "'; " &
                    "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                SeguimientoNoContactado = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(SeguimientoNoContactado, 0)
    End Function
    <WebMethod>
    Public Shared Function Rechazos_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "INNER JOIN DatZendeskTicketsMetrics ON CuboBDLeads.idTicket = DatZendeskTicketsMetrics.TicketId " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
            "AND CuboBDLeads.campaign IN ('15-H','15-SH','715-H') AND CuboBDLeads.result = 'RECHAZO CC' " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND STR_TO_DATE(DatZendeskTicketsMetrics.AssignedAt,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(DatZendeskTicketsMetrics.AssignedAt,'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                Rechazos = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(Rechazos, 0)
    End Function
    <WebMethod>
    Public Shared Function VentaUsuario_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT	COUNT(idTicket) FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "INNER JOIN DatZendeskTicketsMetrics ON CuboBDLeads.idTicket = DatZendeskTicketsMetrics.TicketId " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
            "AND CuboBDLeads.campaign in ('15-H','15-SH','715-H') AND CuboBDLeads.result = 'VENTA' " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND STR_TO_DATE(DatZendeskTicketsMetrics.AssignedAt,'%d/%m/%Y') >= '" & Desde & "' AND STR_TO_DATE(DatZendeskTicketsMetrics.AssignedAt,'%d/%m/%Y') <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                VentaUsuario = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(VentaUsuario, 0)
    End Function
    <WebMethod>
    Public Shared Function OfflineAppsSubmitted_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OfflineAppsSubmitted = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OfflineAppsSubmitted, 0)
    End Function
    <WebMethod>
    Public Shared Function OfflineAppsSubmittedAmex_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'AMEX' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult = 'SOLICITADA' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OfflineAppsSubmittedAmex = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OfflineAppsSubmittedAmex, 0)
    End Function
    <WebMethod>
    Public Shared Function OfflineAppsSubmittedScotia_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'SCOTIA' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OfflineAppsSubmittedScotia = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OfflineAppsSubmittedScotia, 0)
    End Function
    <WebMethod>
    Public Shared Function OfflineAppsSubmittedHSBC_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'HSBC' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 300
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OfflineAppsSubmittedHSBC = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OfflineAppsSubmittedHSBC, 0)
    End Function
    <WebMethod>
    Public Shared Function OfflineAppsApprovedScotia_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'SCOTIA' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult = 'APROBADA' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 1000
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OfflineAppsApprovedScotia = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OfflineAppsApprovedScotia, 0)
    End Function
    <WebMethod>
    Public Shared Function OfflineAppsApprovedHSBC_(ByVal Desde As String, ByVal Hasta As String)
        Dim myConnection = New MySqlConnection("Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;")
        Dim Query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ; " &
            "SELECT COUNT(CuboBDApps.idTicket) FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'HSBC' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult = 'APROBADA' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & Desde & "' AND CuboBDApps.ApplyDate <= '" & Hasta & "'; " &
            "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;"
        Dim dr As MySqlDataReader
        Dim myCommand As MySqlCommand
        Try
            myConnection.Open()
            myCommand = New MySqlCommand(Query, myConnection)
            myCommand.CommandTimeout = 1000
            dr = myCommand.ExecuteReader()
            While dr.Read()
                OfflineAppsApprovedHSBC = dr(0)
            End While
            dr.Close()
        Catch ex As Exception
            Console.Write(ex.Message)
        Finally
            myConnection.Close()
        End Try
        Return FormatNumber(OfflineAppsApprovedHSBC, 0)
    End Function
    <WebMethod>
    Public Shared Function CreateCSVfile(ByVal Tipo As String)
        Dim strFilePath As String = "TotalLeads" & Date.Now.Ticks & ".xlsx"
        Dim Query As String = Nothing

        Select Case Tipo
            Case "TotalLeads"
                strFilePath = "TotalLeads" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT * FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Vertical = 'CREDIT CARDS' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "'; "

            Case "RealLeads"
                strFilePath = "RealLeads" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT	* FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "'; "

            Case "OnlineApplyUsers"
                strFilePath = "OnlineApplyUsers" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT	* FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND " &
            "STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "'; "

            Case "OnlineAuthenticatedUsers"
                strFilePath = "OnlineAuthenticatedUsers" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT	* FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDLeads.AuthenticationStatus in('EXCLUSIÓN','AUTENTICADO') AND CuboBDLeads.AmexShortLead = '' AND CuboBDLeads.AuthenticationDate >= '" & FDesde & "'  AND CuboBDLeads.AuthenticationDate <= '" & FHasta & "'; "

            Case "OnlineAuthenticated600"
                strFilePath = "OnlineAuthenticated600" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT	* FROM CuboBDLeads_tbl AS CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND CuboBDLeads.AuthenticationDate >= '" & FDesde & "' AND " &
            "CuboBDLeads.AuthenticationStatus in('AUTENTICADO') AND CuboBDLeads.AmexShortLead = '' AND CuboBDLeads.result = 'VENTA' AND CuboBDLeads.AuthenticationDate <= '" & FHasta & "'; "

            Case "OnlineAppsCreated"
                strFilePath = "OnlineAppsCreated" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDLeads.AuthenticationStatus in('AUTENTICADO') AND CuboBDLeads.AmexShortLead = '' AND CuboBDLeads.result = 'VENTA' AND CuboBDLeads.AuthenticationDate >= '" & FDesde & "' AND CuboBDLeads.AuthenticationDate <= '" & FHasta & "'; "

            Case "OnlineAppsSubmitted"
                strFilePath = "OnlineAppsSubmitted" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OnlineAppsSubmittedAmex"
                strFilePath = "OnlineAppsSubmittedAmex" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'AMEX' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OnlineAppsSubmittedScotia"
                strFilePath = "OnlineAppsSubmittedScotia" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'SCOTIA' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OnlineAppsSubmittedCiti"
                strFilePath = "OnlineAppsSubmittedCiti" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'CITI' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OnlineAppsSubmittedHSBC"
                strFilePath = "OnlineAppsSubmittedHSBC" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups IN ('AUTENTICACION','DATA INPUT') AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.Bank = 'HSBC' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OnlineAppsApprovedScotia"
                strFilePath = "OnlineAppsApprovedScotia" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE " &
            "CuboBDApps.ApplyResult = 'APROBADA' " &
            "AND CuboBDApps.Bank = 'SCOTIA' " &
            "AND CuboBDLeads.Duplicity = 'NO' " &
            "AND CuboBDLeads.Test = 'NO' " &
            "AND CuboBDLeads.Vertical = 'CREDIT CARDS' " &
            "AND CuboBDLeads.Groups In ('AUTENTICACION','DATA INPUT') " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OnlineAppsApprovedCiti"
                strFilePath = "OnlineAppsApprovedCiti" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE " &
            "CuboBDApps.ApplyResult = 'APROBADA' " &
            "AND CuboBDApps.Bank = 'CITI' " &
            "AND CuboBDLeads.Duplicity = 'NO' " &
            "AND CuboBDLeads.Test = 'NO' " &
            "AND CuboBDLeads.Vertical = 'CREDIT CARDS' " &
            "AND CuboBDLeads.Groups In ('AUTENTICACION','DATA INPUT') " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OnlineAppsApprovedHSBC"
                strFilePath = "OnlineAppsApprovedHSBC" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE " &
            "CuboBDApps.ApplyResult = 'APROBADA' " &
            "AND CuboBDApps.Bank = 'HSBC' " &
            "AND CuboBDLeads.Duplicity = 'NO' " &
            "AND CuboBDLeads.Test = 'NO' " &
            "AND CuboBDLeads.Vertical = 'CREDIT CARDS' " &
            "AND CuboBDLeads.Groups In ('AUTENTICACION','DATA INPUT') " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "AmexShortLeads"
                strFilePath = "AmexShortLeads" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT * FROM CuboBDLeads_tbl AS CuboBDLeads " &
            " WHERE " &
            "CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.AmexShortLead = 'TRUE' " &
            "AND CuboBDLeads.`Fecha Amex Solicitud` >= '" & FDesde & "' " &
            "AND CuboBDLeads.`Fecha Amex Solicitud` <= '" & FHasta & "'; "

            Case "LeadsForOperations"
                strFilePath = "LeadsForOperations" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT * FROM CuboBDLeads WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
        "AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "';"

            Case "Campaings"
                strFilePath = "Campaings" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT * FROM CuboBDLeads WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
        "AND CuboBDLeads.campaign in ('15-H','15-SH','715-H') AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "';"

            Case "SeguimientoNoContactado"
                strFilePath = "SeguimientoNoContactado" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT * FROM CuboBDLeads " &
                    "WHERE CuboBDLeads.Duplicity = 'NO' AND " &
                    "CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
                    "AND CuboBDLeads.campaign in ('15-H','15-SH','715-H') AND " &
                    "CuboBDLeads.result in ('SEGUIMIENTO', 'SIN ATENDER') AND " &
                    "CuboBDLeads.AmexShortLead = '' " &
                    "AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "'; "

            Case "Rechazos"
                strFilePath = "Rechazos" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT * FROM CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
            "AND CuboBDLeads.campaign IN ('15-H','15-SH','715-H') AND CuboBDLeads.result = 'RECHAZO CC' " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "'; "

            Case "VentaUsuario"
                strFilePath = "VentaUsuario" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT	* FROM CuboBDLeads " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' " &
            "AND CuboBDLeads.campaign in ('15-H','15-SH','715-H') AND CuboBDLeads.result = 'VENTA' " &
            "AND CuboBDLeads.AmexShortLead = '' " &
            "AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') >= '" & FDesde & "' AND STR_TO_DATE(CreatedAt_fix,'%d/%m/%Y') <= '" & FHasta & "';"

            Case "OfflineAppsSubmitted"
                strFilePath = "OfflineAppsSubmitted" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OfflineAppsSubmittedAmex"
                strFilePath = "OfflineAppsSubmittedAmex" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'AMEX' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult = 'SOLICITADA' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OfflineAppsSubmittedScotia"
                strFilePath = "OfflineAppsSubmittedScotia" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'SCOTIA' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OfflineAppsSubmittedHSBC"
                strFilePath = "OfflineAppsSubmittedHSBC" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'HSBC' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult <> '' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OfflineAppsApprovedScotia"
                strFilePath = "OfflineAppsApprovedScotia" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'SCOTIA' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult = 'APROBADA' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

            Case "OfflineAppsApprovedHSBC"
                strFilePath = "OfflineAppsApprovedHSBC" & Date.Now.Ticks & ".xlsx"
                Query = "SELECT CuboBDApps.*, CuboBDLeads.* FROM CuboBDApps_tbl AS CuboBDApps " &
            "INNER JOIN CuboBDLeads_tbl AS CuboBDLeads ON CuboBDApps.idTicket = CuboBDLeads.idTicket " &
            "WHERE CuboBDApps.Bank = 'HSBC' AND CuboBDLeads.Duplicity = 'NO' AND CuboBDLeads.Test = 'NO' AND " &
            "CuboBDLeads.Vertical = 'CREDIT CARDS' AND CuboBDLeads.Groups = 'VENTAS CREDIT CARDS' AND " &
            "CuboBDApps.ApplyResult = 'APROBADA' AND CuboBDLeads.AmexShortLead = '' AND " &
            "CuboBDApps.ApplyDate >= '" & FDesde & "' AND CuboBDApps.ApplyDate <= '" & FHasta & "'; "

        End Select
        'Query = TotalLeadsQry
        Dim dtable As New DataTable("Export")
        Dim connectMySQL As String = "Server=cgmysql.cvjskbkfw6ox.us-east-2.rds.amazonaws.com;Database=GuruLake;Uid=sa;Pwd=Inteldba01;"
        Using cn1 As MySqlConnection = New MySqlConnection(connectMySQL)
            cn1.Open()
            Dim cmdSQLite As MySqlCommand = cn1.CreateCommand()
            cmdSQLite.CommandTimeout = 300
            Dim da As New MySqlDataAdapter(cmdSQLite)
            With cmdSQLite
                .CommandType = CommandType.Text
                .CommandText = Query
            End With
            'Dim oDataTable As New DataTable("TMG")
            Dim oAdapter As New MySqlDataAdapter(cmdSQLite)
            Try
                oAdapter.Fill(dtable)
                cn1.Close()
            Catch ex As Exception
                cn1.Close()
                Console.Write(ex.Message)
            End Try
        End Using

        Dim wb = New ClosedXML.Excel.XLWorkbook
        wb.Worksheets.Add(dtable)
        wb.SaveAs("C:\Users\Edson\Documents\CoruAnalytics_git\TempFolder\" & strFilePath)
        'wb.SaveAs("W:\CoruAnalytics\TempFolder\" & strFilePath)
        ReleaseObject(wb)
        GC.Collect()
        'Return "http://localhost:57075/TempFolder/" & strFilePath
        Return "http://analytics.coru.com/TempFolder/" & strFilePath
    End Function
    Private Sub ExportToExcel(ByVal dtTemp As DataTable, ByVal filepath As String)
        Dim strFileName As String = filepath
        Dim _excel As New Excel.Application
        Dim wBook As Excel.Workbook
        Dim wSheet As Excel.Worksheet

        wBook = _excel.Workbooks.Add()
        wSheet = wBook.ActiveSheet()

        Dim dt As System.Data.DataTable = dtTemp
        Dim dc As System.Data.DataColumn
        Dim dr As System.Data.DataRow
        Dim colIndex As Integer = 0
        Dim rowIndex As Integer = 0
        'If CheckBox1.Checked Then
        For Each dc In dt.Columns
                colIndex = colIndex + 1
                wSheet.Cells(1, colIndex) = dc.ColumnName
            Next
        'End If
        For Each dr In dt.Rows
            rowIndex = rowIndex + 1
            colIndex = 0
            For Each dc In dt.Columns
                colIndex = colIndex + 1
                wSheet.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)
            Next
        Next
        wSheet.Columns.AutoFit()
        wBook.SaveAs(strFileName)

        ReleaseObject(wSheet)
        wBook.Close(False)
        ReleaseObject(wBook)
        _excel.Quit()
        ReleaseObject(_excel)
        GC.Collect()

        'MessageBox.Show("File Export Successfully!")
    End Sub
    Private Shared Sub ReleaseObject(ByVal o As Object)
        Try
            While (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0)
            End While
        Catch
        Finally
            o = Nothing
        End Try
    End Sub
    'Method to convert an column number to column name
    Private Shared Function ConvertToLetter(ByVal num As Integer) As String
        num = num - 1
        If num < 0 Or num >= 27 * 26 Then
            ConvertToLetter = "-"
        Else
            If num < 26 Then
                ConvertToLetter = Chr(num + 65)
            Else
                ConvertToLetter = Chr(num \ 26 + 64) + Chr(num Mod 26 + 65)
            End If
        End If
    End Function

    'Method convert datacolumn to array 
    Public Shared Function ToArray(ByVal dr As DataTable) As String()
        Dim ary() As String = Array.ConvertAll(Of DataRow, String)(dr.Select(), AddressOf DataRowToString)
        Return ary
    End Function

    Public Shared Function DataRowToString(ByVal dr As DataRow) As String
        Return dr(strCommonColumnName).ToString()
    End Function

    'Method convert Array to string Public 
    Public Shared Function AryToString(ByVal ary As String()) As String
        Return String.Join(vbNewLine, ToArray(dTable))
    End Function

End Class
